
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Web/Phone")]
	public class PhoneAction : Action
	{
		public static void OpenURL( string phoneNumber )
		{
			if( phoneNumber != "" )
				Application.OpenURL( "tel://" + phoneNumber);
		}


		public string numberString = "";

		public virtual PhoneAction Init( string phoneNumber )
		{
			numberString = phoneNumber;
			return this;
		}

		public virtual void TriggerOpenURLActor( string phoneNumber )
		{
			RunAction();
		}


		protected override void RunAction()
		{
			string num = "";
			try   { num = PullParameter<string>("numberString"); }
			catch { num = numberString; }

			OpenURL(num);
		}
	}
}
