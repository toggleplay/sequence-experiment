
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Web/URL")]
	public class OpenURLAction : Action
	{
		public static void OpenURL( string webURL )
		{
			if( webURL != "" )
				Application.OpenURL(webURL);
		}


		public string url = "";

		public virtual OpenURLAction Init( string webURL )
		{
			url = webURL;
			return this;
		}

		public virtual void TriggerOpenURLActor( string webURL )
		{
			PushParameter( "url", webURL );
			RunAction();
		}

		protected override void RunAction()
		{
			string webURL = "";
			try   { webURL = PullParameter<string>("url"); }
			catch { webURL = url; }
			
			if( webURL == "" )
			{ Debug.LogError("No URL to open web for"); return; }
			
			OpenURL(webURL);
		}
	}
}
