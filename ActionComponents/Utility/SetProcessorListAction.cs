
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Restrictions;


namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Utility/Set Processor List")]
	public class SetProcessorListAction : Action
	{
		public Processors.Processor processor = null;
		public string listName = null;
		public bool beginImmediately = true;

		public virtual SetProcessorListAction Init( Processors.Processor onProcessor, string toList )
		{
			processor = onProcessor;
			listName = toList;
			return this;
		}

		protected override void RunAction()
		{
			if( processor != null )
				processor.SetToList(listName, beginImmediately);
		}
	}
}
