
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Other/Load Scene")]
	public class LoadSceneActions : Action
	{
		public string scene = "";
		public bool additive = false;
		public bool async = false;


		public virtual LoadSceneActions Init( string loadScene )
		{
			scene = loadScene;
			return this;
		}

		public virtual LoadSceneActions Init( string loadScene, bool isAdditive, bool isAsync )
		{
			scene = loadScene;
			additive = isAdditive;
			async = isAsync;
			return this;
		}

		public virtual void TriggerLoadSceneActor( )
		{
			RunAction();
		}
		public virtual void TriggerLoadScene( string loadScene )
		{
			PushParameter( "scene", loadScene );
			RunAction();
		}
		public virtual void TriggerLoadSceneAdditive( string loadScene )
		{
			PushParameter( "scene", loadScene );
			PushParameter( "additive", true );
			PushParameter( "async", false );
			RunAction();
		}
		public virtual void TriggerLoadSceneAsync( string loadScene )
		{
			PushParameter( "scene", loadScene );
			PushParameter( "additive", false );
			PushParameter( "async", true );
			RunAction();
		}
		public virtual void TriggerLoadSceneAdditiveAsync( string loadScene )
		{
			PushParameter( "scene", loadScene );
			PushParameter( "additive", true );
			PushParameter( "async", true );
			RunAction();
		}

		protected override void RunAction()
		{
			// pull parameters
			string loadScene = "";

			try   { loadScene = PullParameter<string>("scene"); }
			catch { loadScene = scene; }
			if( loadScene == "" )
			{ Debug.LogError("Scene to load is not set"); return; }

			bool isAdditive = additive;
			bool isAsync = async;

			try   { isAdditive = PullParameter<bool>("additive"); }
			catch { isAdditive = additive; }
			try   { isAsync = PullParameter<bool>("async"); }
			catch { isAsync = async; }

			if( isAdditive )
			{
				if( isAsync )
					Application.LoadLevelAdditiveAsync(loadScene);
				else
					Application.LoadLevelAdditive(loadScene);
			}
			else if( isAsync )
				Application.LoadLevelAsync(loadScene);
			else
				Application.LoadLevel(loadScene);
		}
	}
}
