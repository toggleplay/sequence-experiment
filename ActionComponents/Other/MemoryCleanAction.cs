
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Other/Clean Memory")]
	public class MemoryCleanAction : Action
	{
		public static void Clean()
		{
			Resources.UnloadUnusedAssets();
			System.GC.Collect();
		}

		public virtual void TriggerMemoryClean( )
		{
			RunAction();
		}


		protected override void RunAction()
		{
			Clean();
		}
	}
}
