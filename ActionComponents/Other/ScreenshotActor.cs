
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Other/Screenshot")]
	public class ScreenshotActor : Action
	{
		public static void TakeScreenshot( int supersize )
		{
			string y = System.DateTime.Now.ToString("(yyyy-MM-dd)");
			string t = System.DateTime.Now.ToString("(HH-mm-ss)");
			string screenshotFilename = "screenshot year" + y + " time" + t + ".png";
			Debug.Log("screenshot saving with: " + screenshotFilename);
			Application.CaptureScreenshot(screenshotFilename, supersize);
		}


		public int screenshotSupersize = 1;

		public virtual ScreenshotActor Init( int supersize )
		{
			screenshotSupersize = supersize;
			return this;
		}

		public virtual void TriggerPlayAnimation( int supersize )
		{
			PushParameter( "supersize", supersize );
			RunAction();
		}


		protected override void RunAction()
		{
			int supSize = 0;
			try   { supSize = PullParameter<int>("supersize"); }
			catch { supSize = screenshotSupersize; }

			TakeScreenshot(supSize);
		}
	}
}
