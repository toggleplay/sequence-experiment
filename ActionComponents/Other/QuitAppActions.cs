
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Other/Quit")]
	public class QuitAppActions : Action
	{
		public virtual void TriggerQuitAppActor( )
		{
			RunAction();
		}


		protected override void RunAction()
		{
			Application.Quit();
		}
	}
}
