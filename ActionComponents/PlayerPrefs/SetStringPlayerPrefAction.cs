
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Player Prefs/String Player Pref")]
	public class SetStringPlayerPrefAction : Action
	{
		public string prefKey = "";
		public string setting = "";

		public virtual SetStringPlayerPrefAction Init( string playerPrefKey, string setTo )
		{
			prefKey = playerPrefKey;
			setting = setTo;
			return this;
		}

		public virtual void TriggerStringPlayerPref( string key, string value )
		{
			PushParameter( "key", key );
			PushParameter( "value", value );
			RunAction();
		}

		protected override void RunAction()
		{
			string key = HasParameter("key") ?
				(string)PullParameter("key") : prefKey;
			if( key == "" )
			{ Debug.LogError("No key to assign PlayerPref for"); return; }
			string val = HasParameter("value") ?
				(string)PullParameter("value") : setting;

			PlayerPrefs.SetString(key, val);
		}
	}
}
