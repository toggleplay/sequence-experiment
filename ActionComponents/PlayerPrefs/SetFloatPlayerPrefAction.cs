
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Player Prefs/Float Player Pref")]
	public class SetFloatPlayerPrefAction : Action
	{
		public string prefKey = "";
		public FloatValue setting = new FloatValue(0);
		public bool additive = false;
		public bool keepInRange = false;
		// TODO better display for this (custom inspector / property drawer)
		public Vector2 range = Vector2.zero;


		public virtual SetFloatPlayerPrefAction Init( string playerPrefKey, float setTo )
		{
			prefKey = playerPrefKey;
			setting = new FloatValue(setTo);
			return this;
		}

		public virtual SetFloatPlayerPrefAction Init( string playerPrefKey, float setTo, bool isAdditive )
		{
			prefKey = playerPrefKey;
			setting = new FloatValue(setTo);
			additive = isAdditive;
			return this;
		}

		public virtual SetFloatPlayerPrefAction Init( string playerPrefKey, float setTo, bool isAdditive, Vector2 clampRange )
		{
			prefKey = playerPrefKey;
			setting = new FloatValue(setTo);
			additive = isAdditive;
			keepInRange = true;
			range = clampRange;
			return this;
		}

		public virtual void TriggerFloatPlayerPref( string key, float value )
		{
			PushParameter( "key", key );
			PushParameter( "value", value );
			RunAction();
		}

		protected override void RunAction()
		{
			// pull parameters
			string key = HasParameter("key") ?
				(string)PullParameter("key") : prefKey;
			if( key == "" )
			{ Debug.LogError("No key to assign PlayerPref for"); return; }
			float val = HasParameter("value") ?
				(float)PullParameter("value") : setting.Value;

			if( additive )
			{
				float current = PlayerPrefs.GetFloat(key);
				current += val;
				if( keepInRange )
					current = Mathf.Clamp(current, range.x, range.y);
				PlayerPrefs.SetFloat(key, current);
			}
			else
			{
				float s = val;
				if( keepInRange )
					s = Mathf.Clamp(s, range.x, range.y);
				PlayerPrefs.SetFloat(key, s);
			}
		}
	}
}
