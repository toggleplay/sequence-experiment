
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Player Prefs/Int Player Pref")]
	public class SetIntPlayerPrefAction : Action
	{
		public string prefKey = "";
		public IntValue setting = new IntValue(0);
		public bool additive = false;
		public bool keepInRange = false;
		public TogglePlay.Serializables.IntPair range = new TogglePlay.Serializables.IntPair();


		public virtual SetIntPlayerPrefAction Init( string playerPrefKey, int setTo )
		{
			prefKey = playerPrefKey;
			setting = new IntValue(setTo);
			return this;
		}

		public virtual SetIntPlayerPrefAction Init( string playerPrefKey, int setTo, bool isAdditive )
		{
			prefKey = playerPrefKey;
			setting = new IntValue(setTo);
			additive = isAdditive;
			return this;
		}

		public virtual SetIntPlayerPrefAction Init( string playerPrefKey, int setTo, bool isAdditive, TogglePlay.Serializables.IntPair clampRange )
		{
			prefKey = playerPrefKey;
			setting = new IntValue(setTo);
			additive = isAdditive;
			keepInRange = true;
			range = clampRange;
			return this;
		}

		public virtual void TriggerIntPlayerPref( string key, int value )
		{
			PushParameter( "key", key );
			PushParameter( "value", value );
			RunAction();
		}

		protected override void RunAction()
		{
			// pull parameters
			string key = HasParameter("key") ?
				(string)PullParameter("key") : prefKey;
			if( key == "" )
			{ Debug.LogError("No key to assign PlayerPref for"); return; }
			int val = HasParameter("value") ?
				(int)PullParameter("value") : setting.Value;

			if( additive )
			{
				int current = PlayerPrefs.GetInt(key);
				current += val;
				if( keepInRange )
					current = Mathf.Clamp(current, range.value1, range.value2);
				PlayerPrefs.SetInt(key, current);
			}
			else
			{
				int setTo = val;
				if( keepInRange )
					setTo = Mathf.Clamp(setTo, range.value1, range.value2);
				PlayerPrefs.SetInt(key, setTo);
			}
		}
	}
}
