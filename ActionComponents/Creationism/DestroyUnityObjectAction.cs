
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Creationism/Destroy")]
	public class DestroyUnityObjectAction : Action
	{
		// A UnityEngine.Object can be a component or GameObject
		public UnityEngine.Object targetObject = null;

		public virtual DestroyUnityObjectAction Init( UnityEngine.Object target )
		{
			targetObject = target;
			return this;
		}

		public virtual void TriggerDestroyObjectActor( )
		{
			RunAction();
		}
		public virtual void TriggerDestroyObject( UnityEngine.Object toDestroy )
		{
			PushParameter( "targetObject", toDestroy );
			RunAction();
		}

		protected override void RunAction()
		{
			// get correct values
			UnityEngine.Object toDestroy = null;

			try   { toDestroy = PullParameter<UnityEngine.Object>("targetObject"); }
			catch { toDestroy = targetObject; }
			if( toDestroy == null )
				return;

			Destroy(toDestroy);
		}
	}
}
