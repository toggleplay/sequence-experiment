
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Creationism/Instantiate")]
	public class InstantiateAction : Action
	{
		public GameObject createPrefab = null;

		public Transform parentTo = null;
		public Transform matchTransformValuesOf = null;
		public TransformBoolean matchTargets = new TransformBoolean(true,true,false);
		
		public bool requireUniformScaleMatcher = true;


		public virtual InstantiateAction Init( GameObject prefab )
		{
			createPrefab = prefab;
			parentTo = null;
			matchTransformValuesOf = null;
			matchTargets = new TransformBoolean();
			return this;
		}
		public virtual InstantiateAction Init( GameObject prefab, Transform parent )
		{
			createPrefab = prefab;
			parentTo = parent;
			matchTransformValuesOf = parent;
			matchTargets = new TransformBoolean(true,true,false);
			return this;
		}
		public virtual InstantiateAction Init( GameObject prefab, Transform parent, bool setToParentTransform )
		{
			createPrefab = prefab;
			parentTo = parent;
			if( setToParentTransform )
				matchTransformValuesOf = parent;
			else
				matchTransformValuesOf = null;
			matchTargets = new TransformBoolean(true,true,false);
			return this;
		}
		public virtual InstantiateAction Init( GameObject prefab, Transform parent, Transform matchToTransform, TransformBoolean matchBy )
		{
			createPrefab = prefab;
			parentTo = parent;
			matchTransformValuesOf = matchToTransform;
			matchTargets = matchBy;
			return this;
		}


		// named for animation events
		public virtual void TriggerInstantiateActor( )
		{
			RunAction();
		}
		public virtual void TriggerInstantiateWithPrefab( GameObject prefab )
		{
			PushParameter( "createPrefab", prefab );
			RunAction();
		}


		public virtual void TriggerInstantiateWithPrefabParent( GameObject prefab, Transform parent )
		{
			PushParameter( "createPrefab", prefab );
			PushParameter( "parentTo", parent );
			RunAction();
		}
		public virtual void TriggerInstantiate( GameObject prefab, Transform parent, Transform matchToTransform, TransformBoolean matchBy )
		{
			PushParameter( "createPrefab", prefab );
			PushParameter( "parentTo", parent );
			PushParameter( "matchTransformValuesOf", matchToTransform );
			PushParameter( "matchTargets", matchBy );
			RunAction();
		}


		protected override void RunAction()
		{
			// get correct values
			GameObject prefab = null;
			try   { prefab = PullParameter<GameObject>("createPrefab"); }
				catch { prefab = createPrefab; }
			if( prefab == null )
			{ Debug.LogError("No Prefab to create"); return; }

			Transform matchTo = null;
			try   { matchTo = PullParameter<Transform>("matchTransformValuesOf"); }
				catch { matchTo = matchTransformValuesOf; }
			Transform parent = null;
			try   { parent = PullParameter<Transform>("parentTo"); }
				catch { parent = parentTo; }
			TransformBoolean targets = null;
			try   { targets = PullParameter<TransformBoolean>("matchTargets"); }
				catch { targets = matchTargets; }


			GameObject createdObject = (GameObject)Instantiate(prefab);
			Transform createdTransform = createdObject.transform;

			if( matchTo != null )
			{
				if( targets.position )
					createdTransform.position = matchTo.position;

				if( targets.rotation )
					createdTransform.rotation = matchTo.rotation;

				if( targets.scale )
				{
					if( requireUniformScaleMatcher )
					{
						bool allow = true;
						// check through values to see if it could be problematic
						float xScale = matchTo.parent.localScale.x;
						float yScale = matchTo.parent.localScale.y;
						float zScale = matchTo.parent.localScale.z;
						while( matchTo.parent != null )
						{
							if( ! Mathf.Approximately(xScale,yScale) || ! Mathf.Approximately(xScale,zScale) || ! Mathf.Approximately(yScale,zScale) )
							{
								Debug.Log("setting scale to localScale of : " + matchTo.name + " : which has parents with a nonuniform scaling");
								allow = false;
								break;
							}
							
							matchTo = matchTo.parent;
							if( matchTo.parent != null )
							{
								xScale = matchTo.parent.localScale.x;
								yScale = matchTo.parent.localScale.y;
								zScale = matchTo.parent.localScale.z;
							}
						}

						if( allow )
							createdTransform.localScale = matchTo.localScale;
					}
					else
						createdTransform.localScale = matchTo.localScale;
				}
			}

			createdTransform.parent = parent;
		}
	}
}
