
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Transform/Detach Children")]
	public class DetachChildrenAction : Action
	{
		public Transform targetTransform = null;

		public virtual DetachChildrenAction Init( Transform target )
		{
			targetTransform = target;
			return this;
		}

		public virtual void TriggerDetachChildrenActor( Transform target )
		{
			PushParameter( "targetTransform", target );
			RunAction();
		}


		protected override void RunAction()
		{
			Transform target = HasParameter("targetTransform") ?
				(Transform)PullParameter("targetTransform") : targetTransform;

			target.DetachChildren();
		}
	}
}
