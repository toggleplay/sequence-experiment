
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Transform/Set Parent")]
	public class SetParentTransformAction : Action
	{
		public Transform assignAsChild = null;
		public Transform assignAsParent = null;

		protected virtual void Reset()
		{
			if( assignAsChild == null )
				assignAsChild = this.transform;
		}

		public virtual SetParentTransformAction Init( Transform parent )
		{
			if( assignAsChild == null )
				assignAsChild = this.transform;

			assignAsParent = parent;

			return this;
		}
		public virtual SetParentTransformAction Init( Transform child, Transform parent )
		{
			assignAsChild = child;
			assignAsParent = parent;
			
			return this;
		}

		public virtual void TriggerFaceTransform( Transform parent )
		{
			PushParameter( "assignAsParent", parent );
			RunAction();
		}
		public virtual void TriggerFaceTransform( Transform child, Transform parent )
		{
			PushParameter( "assignAsChild", child );
			PushParameter( "assignAsParent", parent );
			RunAction();
		}

		protected override void RunAction()
		{
			Transform childTransform = HasParameter("assignAsChild") ?
				(Transform)PullParameter("assignAsChild") : assignAsChild;
			if( childTransform == null )
			{ Debug.LogError("No child to set parent to"); return; }
			Transform parentTransform = HasParameter("assignAsParent") ?
				(Transform)PullParameter("assignAsParent") : assignAsParent;
			if( parentTransform == null )
			{ Debug.LogError("No parent to set child to"); return; }

			childTransform.parent = parentTransform;
		}
	}
}
