
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Activity/Set MonoBehaviour Activity")]
	public class MonoBehaviourActivityAction : BaseComponentActivityAction<MonoBehaviour>
	{
		// triggers named available for animation events
		public virtual void TriggerMonoBehaviourActivityAction( )
		{
			RunAction();
		}
		public virtual void TriggerMonoBehaviourActivityWithActivity( EActivityType activity )
		{
			PushParameter( "setActivity", activity );
			RunAction();
		}

		protected override void SetActivity(MonoBehaviour target, EActivityType activity)
		{
			switch( activity )
			{
			case EActivityType.setActive:
				target.enabled = true;
				break;
			case EActivityType.setInactive:
				target.enabled = false;
				break;
			case EActivityType.toggleActivity:
				target.enabled = ! target.enabled;
				break;
			}
		}
	}
}