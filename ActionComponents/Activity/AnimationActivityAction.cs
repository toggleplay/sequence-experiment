
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Activity/Set Animation Activity")]
	public class AnimationActivityAction : BaseComponentActivityAction<Animation>
	{
		// triggers available for animation events
		public virtual void TriggerAnimationActivityAction( )
		{
			RunAction();
		}
		public virtual void TriggerAnimationActivity( EActivityType activity )
		{
			PushParameter( "setActivity", activity );
			RunAction();
		}
		
		protected override void SetActivity( Animation target, EActivityType activity )
		{
			switch( activity )
			{
			case EActivityType.setActive:
				target.enabled = true;
				break;
			case EActivityType.setInactive:
				target.enabled = false;
				break;
			case EActivityType.toggleActivity:
				target.enabled = ! target.enabled;
				break;
			}
		}
	}
}