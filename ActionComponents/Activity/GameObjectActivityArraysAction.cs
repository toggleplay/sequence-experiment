
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Activity/Set GameObject[] Activity")]
	public class GameObjectActivityArraysAction : Action
	{
		public GameObject[] activateArray;
		public GameObject[] deactivateArray;
#if (UNITY_3_5 || UNITY_3_0 || UNITY_3_0_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4)
		public bool recursive = true;
#endif

		public virtual GameObjectActivityArraysAction Init( GameObject[] toActivate, GameObject[] toDeactivate )
		{
			activateArray = toActivate;
			deactivateArray = toDeactivate;
			return this;
		}

		public virtual void TriggerGameObjectActivity( GameObject[] toActivate, GameObject[] toDeactivate )
		{
			PushParameter( "activateArray", toActivate );
			PushParameter( "deactivateArray", toDeactivate );
			RunAction();
		}

		protected override void RunAction()
		{
			// get correct values
			GameObject[] toActivate = null;
			GameObject[] toDeactivate = null;
			
			try   { toActivate = PullParameter<GameObject[]>("activateArray"); }
			catch { toActivate = activateArray; }
			if( toActivate == null )
				return;

			try   { toDeactivate = PullParameter<GameObject[]>("deactivateArray"); }
			catch { toDeactivate = deactivateArray; }
			if( toDeactivate == null )
				return;

#if !(UNITY_3_5 || UNITY_3_0 || UNITY_3_0_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4)
			foreach( GameObject g in toActivate )
				g.SetActive(true);
			foreach( GameObject g in toDeactivate )
				g.SetActive(false);
#else
			if( recursive )
			{
				foreach( GameObject g in toActivate )
					g.SetActiveRecursively(true);
				foreach( GameObject g in toDeactivate )
					g.SetActiveRecursively(false);
			}
			else
			{
				foreach( GameObject g in toActivate )
					g.active = true;
				foreach( GameObject g in toDeactivate )
					g.active = false;
			}
#endif
		}
	}
}