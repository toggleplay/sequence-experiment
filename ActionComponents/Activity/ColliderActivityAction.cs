
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Activity/Set Collider Activity")]
	public class ColliderActivityAction : BaseComponentActivityAction<Collider>
	{
		// triggers available for animation events
		public virtual void TriggerColliderActivityAction( )
		{
			RunAction();
		}
		public virtual void TriggerColliderActivity( EActivityType activity )
		{
			PushParameter( "setActivity", activity );
			RunAction();
		}

		protected override void SetActivity( Collider target, EActivityType activity )
		{
			switch( activity )
			{
			case EActivityType.setActive:
				target.enabled = true;
				break;
			case EActivityType.setInactive:
				target.enabled = false;
				break;
			case EActivityType.toggleActivity:
				target.enabled = ! target.enabled;
				break;
			}
		}
	}
}