
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Activity/Set Renderer Activity")]
	public class RendererActivityAction : BaseComponentActivityAction<Renderer>
	{
		// triggers available for animation events
		public virtual void TriggerRendererActivityAction( )
		{
			RunAction();
		}
		public virtual void TriggerRendererActivityWithActivity( EActivityType activity )
		{
			PushParameter( "setActivity", activity );
			RunAction();
		}

		protected override void SetActivity( Renderer target, EActivityType activity )
		{
			switch( activity )
			{
			case EActivityType.setActive:
				target.enabled = true;
				break;
			case EActivityType.setInactive:
				target.enabled = false;
				break;
			case EActivityType.toggleActivity:
				target.enabled = ! target.enabled;
				break;
			}
		}
	}
}