
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Actions
{
	public class BaseComponentActivityAction<T> : Action where T : Component
	{
		public enum EActivityType { setActive = 0, setInactive, toggleActivity };
		public T targetElement = null;
		public EActivityType activityType = EActivityType.setActive;

		public virtual BaseComponentActivityAction<T> Init( EActivityType activity )
		{
			if( targetElement == null )
				targetElement = this.GetComponent<T>();
			activityType = activity;
			return this;
		}
		public virtual BaseComponentActivityAction<T> Init( T target, EActivityType activity )
		{
			targetElement = target;
			activityType = activity;
			return this;
		}

		public virtual void TriggerActivityAction( )
		{
			RunAction();
		}
		public virtual void TriggerActivity( EActivityType activity )
		{
			PushParameter( "setActivity", activity );
			RunAction();
		}
		public virtual void TriggerActivity( T target, EActivityType activity )
		{
			PushParameter( "target", target );
			PushParameter( "setActivity", activity );
			RunAction();
		}

		protected override void RunAction( )
		{
			// get correct values
			T target = null;
			EActivityType activity = EActivityType.setActive;

			try   { target = PullParameter<T>("target"); }
			catch { target = targetElement; }
			if( target == null )
				return;

			try   { activity = PullParameter<EActivityType>("setActivity"); }
			catch { activity = activityType; }

			SetActivity( target, activity );
		}

		protected virtual void SetActivity( T target, EActivityType activity )
		{
		}
	}
}