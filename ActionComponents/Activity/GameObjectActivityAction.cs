
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace TogglePlay.Actions
{
	[AddComponentMenu("Toggle Play/Actions/Activity/Set GameObject Activity")]
	public class GameObjectActivityAction : Action
	{
		public GameObject targetObject = null;
		public bool setActivity = true;

#if (UNITY_3_5 || UNITY_3_0 || UNITY_3_0_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4)
		public bool recursive = true;
#endif

		public virtual GameObjectActivityAction Init( bool activity )
		{
			if( targetObject == null )
				targetObject = this.gameObject;
			setActivity = activity;
			return this;
		}
		public virtual GameObjectActivityAction Init( GameObject target, bool activity )
		{
			targetObject = target;
			setActivity = activity;
			return this;
		}

		public virtual void TriggerGameObjectActivityAction( )
		{
			RunAction();
		}
		public virtual void TriggerGameObjectActivityWithActivity( bool activity )
		{
			PushParameter( "setActivity", activity );
			RunAction();
		}
		public virtual void TriggerGameObjectActivity( GameObject target, bool activity )
		{
			PushParameter( "targetObject", target );
			PushParameter( "setActivity", activity );
			RunAction();
		}

		protected override void RunAction()
		{
			// get correct values
			GameObject target = null;
			bool activity = true;
			
			try   { target = PullParameter<GameObject>("targetObject"); }
			catch { target = targetObject; }
			if( target == null )
				return;
			
			try   { activity = PullParameter<bool>("setActivity"); }
			catch { activity = setActivity; }

#if !(UNITY_3_5 || UNITY_3_0 || UNITY_3_0_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4)
			target.SetActive(activity);
#else
			if( recursive )
				target.SetActiveRecursively(activity);
			else
				target.active = activity;
#endif
		}
	}
}