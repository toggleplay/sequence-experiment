using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Render Message")]
	public class OnRenderMessagesMessenger : EventMessenger
	{
		public enum EProcessOn { preRender = 0, preCull, postRender, willRenderObject, renderObject };
		public EProcessOn processOn = EProcessOn.preRender;

		void OnPreRender()
		{
			if( processOn == EProcessOn.preRender )
				Send();
		}

		void OnPostRender()
		{
			if( processOn == EProcessOn.postRender )
				Send();
		}

		void OnPreCull()
		{
			if( processOn == EProcessOn.preCull )
				Send();
		}

		void OnWillRenderObject()
		{
			if( processOn == EProcessOn.willRenderObject )
				Send();
		}

		void OnRenderObject()
		{
			if( processOn == EProcessOn.renderObject )
				Send();
		}
	}
}