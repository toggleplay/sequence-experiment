using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Application Event")]
	public class OnApplicationEventMessenger : CoreLibMessenger
	{
		// TODO editor script for this
		public enum EProcessOn  { onFocus = 0, onQuit, onPause };
		public enum EFocusState { intoFocus = 0, outOfFocus };
		public enum EPauseState { intoPause = 0, outOfPause };

		public EProcessOn  processOn = EProcessOn.onPause;
		public EFocusState onFocalState = EFocusState.intoFocus;
		public EPauseState onPause = EPauseState.intoPause;

		void OnApplicationFocus( bool focalStatus )
		{
			if( processOn == EProcessOn.onFocus )
			{
				if( onFocalState == EFocusState.intoFocus && focalStatus == true )
					Send();
				else if( onFocalState == EFocusState.outOfFocus && focalStatus == false )
					Send();
			}
		}

		void OnApplicationQuit( )
		{
			if( processOn == EProcessOn.onQuit )
				Send();
		}
		
		void OnApplicationPause( bool enterPause )
		{
			if( processOn == EProcessOn.onPause )
			{
				if( onPause == EPauseState.intoPause && enterPause == true )
					Send();
				else if( onPause == EPauseState.outOfPause && enterPause == false )
					Send();
			}
		}
	}
}