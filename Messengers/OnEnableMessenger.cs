using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Enable")]
	public class OnEnableMessenger : EventMessenger
	{
		protected void OnEnable()
		{
			Send();
		}
	}
}