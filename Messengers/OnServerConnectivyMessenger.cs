using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Server Connectivity")]
	public class OnServerConnectivityMessenger : EventMessenger
	{
		public enum EProcessOn { connectedToServer = 0, serverInitialised, disconnectedFromServer };
		public EProcessOn processOn = EProcessOn.connectedToServer;

		void OnConnectedToServer()
		{
			if( processOn == EProcessOn.connectedToServer )
				Send();
		}

		void OnServerInitialised()
		{
			if( processOn == EProcessOn.serverInitialised )
				Send();
		}

		void OnDisconnectedFromServer(NetworkDisconnection disConInfo)
		{
			if( processOn == EProcessOn.disconnectedFromServer )
				Send();
		}
	}
}