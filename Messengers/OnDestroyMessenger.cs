using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Destroy")]
	public class OnDestroyMessenger : EventMessenger
	{
		void OnDestroy()
		{
			Send();
		}
	}
}