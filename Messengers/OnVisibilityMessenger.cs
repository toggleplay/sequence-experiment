using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Visibility")]
	public class OnVisibilityMessenger : EventMessenger
	{
		public enum EVisibleState { onVisible = 0, onInvisible };
		public EVisibleState onState = EVisibleState.onVisible;

		void OnBecameVisible()
		{
			if( onState == EVisibleState.onVisible )
				Send();
		}

		void OnBecameInvisible()
		{
			if( onState == EVisibleState.onInvisible )
				Send();
		}
	}
}