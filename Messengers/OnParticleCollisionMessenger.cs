using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Particle Collision")]
	public class OnParticleCollisionMessenger : EventMessenger
	{
		void OnParticleCollision()
		{
			Send();
		}
	}
}