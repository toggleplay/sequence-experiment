using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Mouse")]
	public class OnMouseMessenger : EventMessenger
	{
		public enum EMouseEvent { Enter = 0, Exit, Over, Up, Down, Drag, UpAsButton };
		public EMouseEvent onMouse = EMouseEvent.Down;

		void OnMouseDown()
		{
			if( onMouse == EMouseEvent.Down )
				Send();
		}

		void OnMouseDrag()
		{
			if( onMouse == EMouseEvent.Drag )
				Send();
		}

		void OnMouseEnter()
		{
			if( onMouse == EMouseEvent.Enter )
				Send();
		}

		void OnMouseExit()
		{
			if( onMouse == EMouseEvent.Exit )
				Send();
		}

		void OnMouseOver()
		{
			if( onMouse == EMouseEvent.Over )
				Send();
		}

		void OnMouseUp()
		{
			if( onMouse == EMouseEvent.Up )
				Send();
		}

		void OnMouseUpAsButton()
		{
			if( onMouse == EMouseEvent.UpAsButton )
				Send();
		}
	}
}