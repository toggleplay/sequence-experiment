using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Start")]
	public class OnStartMessenger : EventMessenger
	{
		void Start()
		{
			Send();
		}
	}
}