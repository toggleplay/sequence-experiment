using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Player Connected")]
	public class OnPlayerConnectedMessenger : EventMessenger
	{
		public enum EProcessOn { connected = 0, disconnected };
		public EProcessOn processOn = EProcessOn.connected;

		void OnPlayerConnected()
		{
			if( processOn == EProcessOn.connected )
				Send();
		}

		void OnPlayerDisconnected()
		{
			if( processOn == EProcessOn.disconnected )
				Send();
		}
	}
}