using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Camera Change")]
	public class OnCameraChangeMessenger : EventMessenger
	{
		// TODO editscript same as the processor
		public enum EBeginOnCameraChange { FOV = 1, orthographicSize = 2, clippingPlanes = 4, depth = 8, viewport = 16 };
		public static bool IsSet(EBeginOnCameraChange flags, EBeginOnCameraChange flag)
		{
			int flagsValue = (int)(EBeginOnCameraChange)flags;
			int flagValue = (int)(EBeginOnCameraChange)flag;
			
			return (flagsValue & flagValue) != 0;
		}

		public Camera cameraToWatch = null;
		[HideInInspector, SerializeField] protected EBeginOnCameraChange onChangeMask = 0;

		protected float prevFOV = -1;
		protected float prevOrthoSize = -1;
		protected float prevDepth = -1;
		protected float prevNearClipPlane = -1;
		protected float prevFarClipPlane = -1;
		protected float prevRectX = -1;
		protected float prevRectY = -1;
		protected float prevRectWidth = -1;
		protected float prevRectHeight = -1;

		void Reset()
		{
			if( cameraToWatch == null )
				cameraToWatch = this.GetComponent<Camera>();
		}

		protected void OnEnable()
		{
			if( cameraToWatch == null )
				return;

			prevFOV = cameraToWatch.fieldOfView;
			prevDepth = cameraToWatch.depth;
			prevNearClipPlane = cameraToWatch.nearClipPlane;
			prevFarClipPlane = cameraToWatch.farClipPlane;
			prevOrthoSize = cameraToWatch.orthographicSize;
			prevRectX = cameraToWatch.rect.x;
			prevRectY = cameraToWatch.rect.y;
			prevRectHeight = cameraToWatch.rect.height;
			prevRectWidth = cameraToWatch.rect.width;
		}

		void Update()
		{
			if( IsSet(onChangeMask, EBeginOnCameraChange.FOV ) )
			{
				if( ! Mathf.Approximately(prevFOV, cameraToWatch.fieldOfView) )
				{
					prevFOV = cameraToWatch.fieldOfView;
					Send();
				}
			}
			else if( IsSet(onChangeMask, EBeginOnCameraChange.orthographicSize ) )
			{
				if( ! Mathf.Approximately(prevOrthoSize, cameraToWatch.orthographicSize) )
				{
					prevOrthoSize = cameraToWatch.orthographicSize;
					Send();
				}
			}
			else if( IsSet(onChangeMask, EBeginOnCameraChange.clippingPlanes ) )
			{
				if( ! Mathf.Approximately(prevNearClipPlane, cameraToWatch.nearClipPlane)
				   || ! Mathf.Approximately(prevFarClipPlane, cameraToWatch.farClipPlane) )
				{
					prevNearClipPlane = cameraToWatch.nearClipPlane;
					prevFarClipPlane = cameraToWatch.farClipPlane;
					Send();
				}
			}
			else if( IsSet(onChangeMask, EBeginOnCameraChange.depth ) )
			{
				if( ! Mathf.Approximately(prevDepth, cameraToWatch.depth) )
				{
					prevDepth = cameraToWatch.depth;
					Send();
				}
			}
			else if( IsSet(onChangeMask, EBeginOnCameraChange.viewport ) )
			{
				if( ! Mathf.Approximately(prevRectX, cameraToWatch.rect.x)
				   || ! Mathf.Approximately(prevRectY, cameraToWatch.rect.y)
				   || ! Mathf.Approximately(prevRectHeight, cameraToWatch.rect.height)
				   || ! Mathf.Approximately(prevRectWidth, cameraToWatch.rect.width) )
				{
					prevRectX = cameraToWatch.rect.x;
					prevRectY = cameraToWatch.rect.y;
					prevRectHeight = cameraToWatch.rect.height;
					prevRectWidth = cameraToWatch.rect.width;
					Send();
				}
			}
		}
	}
}