using UnityEngine;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Keycode")]
	public class OnKeycodeMessenger : EventMessenger
	{
		public enum EKeyState { down = 0, held, up };
		
		public KeyCode onCode = KeyCode.Space;
		public EKeyState onState = EKeyState.down;
		
		void Update()
		{
			switch( onState )
			{
			case EKeyState.down:
				if( Input.GetKeyDown(onCode) )
					Send();
				break;
			case EKeyState.up:
				if( Input.GetKeyUp(onCode) )
					Send();
				break;
			case EKeyState.held:
				if( Input.GetKey(onCode) )
					Send();
				break;
			}
		}
	}
}
