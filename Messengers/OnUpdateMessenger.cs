using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Update")]
	public class OnUpdateMessenger : EventMessenger
	{
		public enum EProcessOn { update = 0, lateUpdate, fixedUpdate };
		public EProcessOn processOn = EProcessOn.update;

		void Update()
		{
			if( processOn == EProcessOn.update )
				Send();
		}

		void LateUpdate()
		{
			if( processOn == EProcessOn.lateUpdate )
				Send();
		}

		void FixedUpdate()
		{
			if( processOn == EProcessOn.fixedUpdate )
				Send();
		}
	}
}