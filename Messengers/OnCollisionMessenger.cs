using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Collision")]
	public class OnCollisionMessenger : EventMessenger
	{
		public string triggerTag = "";
		public enum ETrigger { enter = 0, stay, exit };
		public ETrigger trigger = ETrigger.enter;

		public virtual void OnCollisionEnter( Collision col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.enter )
				Send();
		}
		public virtual void OnCollisionStay( Collision col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.stay )
				Send();
		}
		public virtual void OnCollisionExit( Collision col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.exit )
				Send();
		}
	}
}