using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Disable")]
	public class OnDisableMessenger : EventMessenger
	{
		void OnDisable()
		{
			Send();
		}
	}
}