using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Trigger")]
	public class OnTriggerMessenger : EventMessenger
	{
		public string triggerTag = "";
		public enum ETrigger { enter = 0, stay, exit };
		public ETrigger trigger = ETrigger.enter;

		public virtual void OnTriggerEnter( Collider col )
		{
			if( triggerTag == col.tag && trigger == ETrigger.enter )
				Send();
		}
		public virtual void OnTriggerStay( Collider col )
		{
			if( triggerTag == col.tag && trigger == ETrigger.stay )
				Send();
		}
		public virtual void OnTriggerExit( Collider col )
		{
			if( triggerTag == col.tag && trigger == ETrigger.exit )
				Send();
		}
	}
}