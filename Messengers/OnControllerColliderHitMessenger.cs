using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Controller Collider Hit")]
	public class OnControllerColliderHitMessenger : EventMessenger
	{

		public string triggerTag = "";

		void OnControllerColliderHit(ControllerColliderHit hit)
		{
			if( hit.collider.tag == triggerTag )
				Send();
		}
	}
}