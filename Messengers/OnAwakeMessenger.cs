using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Awake")]
	public class OnAwakeMessenger : EventMessenger
	{
		void Awake()
		{
			Send();
		}
	}
}