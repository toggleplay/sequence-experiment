using UnityEngine;
using System.Collections;

namespace TogglePlay.Messengers
{
	[AddComponentMenu("Toggle Play/Messengers/On Collision 2D")]
	public class OnCollision2DMessenger : EventMessenger
	{
		public string triggerTag = "";
		public enum ETrigger { enter = 0, stay, exit };
		public ETrigger trigger = ETrigger.enter;

		public virtual void OnCollisionEnter2D( Collision2D col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.enter )
				Send();
		}
		public virtual void OnCollisionStay2D( Collision2D col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.stay )
				Send();
		}
		public virtual void OnCollisionExit2D( Collision2D col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.exit )
				Send();
		}
	}
}