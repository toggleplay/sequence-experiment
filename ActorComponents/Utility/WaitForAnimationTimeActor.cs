
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Utility/Wait For Anim Time")]
	public class WaitForAnimationTimeActor : Actor
	{
		public float waitTime = 0;
		public Animation targetAnimation = null;
		public string clipName = "";

		protected virtual void Reset()
		{
			if( targetAnimation == null )
				targetAnimation = this.GetComponent<Animation>();
		}

		public virtual WaitForAnimationTimeActor Init( float waitFor )
		{
			if( targetAnimation == null )
				targetAnimation = this.GetComponent<Animation>();
			waitTime = waitFor;
			return this;
		}

		public virtual WaitForAnimationTimeActor Init( float waitFor, string clip )
		{
			if( targetAnimation == null )
				targetAnimation = this.GetComponent<Animation>();
			waitTime = waitFor;
			clipName = clip;
			return this;
		}

		public virtual WaitForAnimationTimeActor Init( float waitFor, Animation target, string clip )
		{
			targetAnimation = target;
			waitTime = waitFor;
			clipName = clip;
			return this;
		}

		protected override IEnumerator PerformActionRoutine()
		{
			if( targetAnimation == null )
				yield return new Yield.ErrorBreak("No animation to look for");
			if( clipName == "" )
				yield return new Yield.ErrorBreak("No clip to wait on");
			if( targetAnimation.GetClip(clipName) == null )
				yield return new Yield.ErrorBreak("Animation does not contain clip");

			while( targetAnimation[clipName].time < waitTime )
			{
				yield return null;
			}
		}
	}
}
