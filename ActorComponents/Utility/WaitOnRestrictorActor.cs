
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Restrictions;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Utility/Wait For Restrictor")]
	public class WaitOnRestrictorActor : Actor
	{
		public Restrictor waitRestrictor = null;

		public virtual WaitOnRestrictorActor Init( Restrictor waitFor )
		{
			waitRestrictor = waitFor;
			return this;
		}

		protected override IEnumerator PerformActionRoutine()
		{
			if( waitRestrictor == null )
				yield return new Yield.ErrorBreak("No Restrictor to wait for");

			while( waitRestrictor.isRestricted )
			{
				yield return null;
			}
		}
	}
}
