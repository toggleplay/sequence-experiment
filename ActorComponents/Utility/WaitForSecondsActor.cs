
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Utility/Wait For Seconds")]
	public class WaitForSecondsActor : DeltaActor
	{
		public float waitTime = 0;

		public virtual WaitForSecondsActor Init( float waitFor )
		{
			waitTime = waitFor;
			return this;
		}

		protected override IEnumerator PerformActionRoutine()
		{
			float t = 0;
			while( t < waitTime )
			{
				t += delta;
				yield return null;
			}
		}
	}
}
