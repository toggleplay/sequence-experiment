
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Transform/Copy From Other")]
	public class CopyValuesFromActor : Actor
	{
		public Transform targetTransform = null;
		public Transform sourceTransform = null;
		public TransformBoolean copyValuesOf = new TransformBoolean();
		public bool localSpace = true;

		protected virtual void Reset()
		{
			if( targetTransform == null )
				targetTransform = this.transform;
		}

		public virtual CopyValuesFromActor Init( Transform fromOtherTransform )
		{
			if( targetTransform == null )
				targetTransform = this.transform;

			sourceTransform = fromOtherTransform;

			return this;
		}
		public virtual CopyValuesFromActor Init( Transform toSetTransform, Transform fromOtherTransform )
		{
			targetTransform = toSetTransform;
			sourceTransform = fromOtherTransform;
			
			return this;
		}
		public virtual CopyValuesFromActor Init( Transform toSetTransform, Transform fromOtherTransform, TransformBoolean ofValues )
		{
			targetTransform = toSetTransform;
			sourceTransform = fromOtherTransform;
			copyValuesOf = ofValues;
			
			return this;
		}

		public virtual Coroutine TriggerFaceTransform( Transform fromOtherTransform )
		{
			PushParameter( "sourceTransform", fromOtherTransform );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerFaceTransform( Transform fromOtherTransform, TransformBoolean ofValues )
		{
			PushParameter( "sourceTransform", fromOtherTransform );
			PushParameter( "copyValuesOf", ofValues );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerFaceTransform( Transform toSetTransform, Transform fromOtherTransform, TransformBoolean ofValues )
		{
			PushParameter( "targetTransform", toSetTransform );
			PushParameter( "sourceTransform", fromOtherTransform );
			PushParameter( "copyValuesOf", ofValues );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			Transform target = HasParameter("targetTransform") ?
				(Transform)PullParameter("targetTransform") : targetTransform;
			if( target == null )
				yield return new Yield.ErrorBreak("No target to copy values to");
			Transform source = HasParameter("sourceTransform") ?
				(Transform)PullParameter("sourceTransform") : sourceTransform;
			if( source == null )
				yield return new Yield.ErrorBreak("No source to copy values from");
			TransformBoolean toCopy = HasParameter("copyValuesOf") ?
				(TransformBoolean)PullParameter("copyValuesOf") : copyValuesOf;

			if( toCopy.position )
			{
				if( localSpace )
					target.localPosition = source.localPosition;
				else
					target.position = source.position;
			}
			if( toCopy.rotation )
			{
				if( localSpace )
					target.localRotation = source.localRotation;
				else
					target.rotation = source.rotation;
			}
			if( toCopy.scale )
			{
				if( localSpace )
					target.localScale = source.localScale;
				else
					Debug.LogWarning("Cannot tranfer non-local scale");
			}
		}
	}
}
