
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;
using TogglePlay.Serializables;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Transform/Set Local Rotation")]
	public class SetLocalRotationActor : DeltaActor
	{
		public Transform targetTransform = null;
		// TODO make toEuler a Vector3Value
		public Vector3 toEuler = Vector3.zero;
		public EQuaternionInterpolateMethod quaterninoInterpolation = EQuaternionInterpolateMethod.slerp;
		
		protected virtual void Reset()
		{
			if( targetTransform == null )
				targetTransform = this.transform;
		}
		
		public virtual SetLocalRotationActor Init( Transform target, Vector3 targetEuler )
		{
			targetTransform = target;
			toEuler = targetEuler;
			return this;
		}
		
		public virtual SetLocalRotationActor Init( Transform target, Vector3 targetEuler, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			targetTransform = target;
			toEuler = targetEuler;
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}
		
		public virtual Coroutine TriggerLocalRotation( Vector3 targetEuler )
		{
			PushParameter( "toEuler", targetEuler );
			return RunAsCoroutine();
		}
		
		public virtual Coroutine TriggerLocalPosition( Transform target, Vector3 targetEuler )
		{
			PushParameter( "toEuler", targetEuler );
			PushParameter( "targetTransform", target );
			return RunAsCoroutine();
		}
		
		protected override IEnumerator PerformActionRoutine()
		{
			// pull parameters
			Transform target = HasParameter("targetTransform") ?
				(Transform)PullParameter("targetTransform") : targetTransform;
			if( target == null )
				yield return new Yield.ErrorBreak("Target Transform not set");
			Vector3 targetEuler = HasParameter("toEuler") ?
				(Vector3)PullParameter("toEuler") : toEuler;
			
			Quaternion toQuaternion = Quaternion.Euler(targetEuler);
			
			if( deltaType != EUnitDeltaSetting.immediate )
			{
				float lerpedBy = 0;
				
				// get the speed offset from a full 360 revolution
				float rotSpeed = Quaternion.Angle( target.localRotation, toQuaternion );
				if( Mathf.Approximately( rotSpeed, 0 ) )
				{
					target.rotation = toQuaternion;
					yield break;
				}
				rotSpeed = 360f/rotSpeed;
				
				while( true )
				{
					// move closer by delta
					float jump = rotSpeed * (delta/360f);
					lerpedBy += jump;
					
					// when gets more then toBeMovedBy break
					if( lerpedBy > 1f )
						break;
					
					float activeLerp = jump / ( 1f-lerpedBy );
					if( quaterninoInterpolation == EQuaternionInterpolateMethod.lerp )
						target.localRotation = Quaternion.Lerp(target.rotation, toQuaternion, activeLerp);
					else if( quaterninoInterpolation == EQuaternionInterpolateMethod.slerp )
						target.localRotation = Quaternion.Slerp(target.rotation, toQuaternion, activeLerp);

					yield return null;
				}
			}
			
			target.localRotation = toQuaternion;
		}
	}
}
