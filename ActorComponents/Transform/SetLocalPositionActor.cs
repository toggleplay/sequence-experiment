
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Transform/Set Local Position")]
	public class SetLocalPositionActor : DeltaActor
	{
		public Transform targetTransform = null;
		// TODO make toPosition a Vector3Value
		public Vector3 toLocalPosition = Vector3.zero;
		Vector3 currentPosition = Vector3.zero;

		protected virtual void Reset()
		{
			if( targetTransform == null )
				targetTransform = this.transform;
		}


		public virtual SetLocalPositionActor Init( Transform target, Vector3 targetPosition )
		{
			targetTransform = target;
			toLocalPosition = targetPosition;
			return this;
		}

		public virtual SetLocalPositionActor Init( Transform target, Vector3 targetPosition, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			targetTransform = target;
			toLocalPosition = targetPosition;
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}

		public virtual Coroutine TriggerLocalPosition( Vector3 targetPosition )
		{
			PushParameter( "toLocalPosition", targetPosition );
			return RunAsCoroutine();
		}

		public virtual Coroutine TriggerLocalPosition( Transform target, Vector3 targetPosition )
		{
			PushParameter( "toLocalPosition", targetPosition );
			PushParameter( "targetTransform", target );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			// pull parameters
			Transform target = HasParameter("targetTransform") ?
				(Transform)PullParameter("targetTransform") : targetTransform;
			if( targetTransform == null )
				yield return new Yield.ErrorBreak("Target Transform not set");
			Vector3 targetPosition = HasParameter("toLocalPosition") ?
				(Vector3)PullParameter("toLocalPosition") : toLocalPosition;

			if( deltaType != EUnitDeltaSetting.immediate )
			{
				currentPosition = target.localPosition;
				Vector3 unitMovement = targetPosition - currentPosition;
				float toBeMovedBy = unitMovement.magnitude;
				unitMovement.Normalize();
				float beenMovedBy = 0;
				if( Mathf.Approximately(toBeMovedBy,0) )
					yield return Yield.completed;

				Vector3 jump = Vector3.zero;

				while( true )
				{
					// move closer by delta
					jump = unitMovement * delta;
					beenMovedBy += jump.magnitude;

					// when gets more then toBeMovedBy break
					if( beenMovedBy > toBeMovedBy )
						break;

					currentPosition += jump;
					target.localPosition = currentPosition;
					yield return null;
				}
			}

			target.localPosition = targetPosition;
		}
	}
}
