
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Transform/Set Local Scale")]
	public class SetLocalScaleActor : DeltaActor
	{
		public Transform targetTransform = null;
		// TODO make toScale a Vector3Value
		public Vector3 toLocalScale = Vector3.zero;
		Vector3 currentScale = Vector3.zero;

		protected virtual void Reset()
		{
			if( targetTransform == null )
				targetTransform = this.transform;
		}


		public virtual SetLocalScaleActor Init( Transform target, Vector3 targetScale )
		{
			targetTransform = target;
			toLocalScale = targetScale;
			return this;
		}

		public virtual SetLocalScaleActor Init( Transform target, Vector3 targetScale, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			targetTransform = target;
			toLocalScale = targetScale;
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}

		public virtual Coroutine TriggerLocalScale( Vector3 targetScale )
		{
			PushParameter( "toLocalScale", targetScale );
			return RunAsCoroutine();
		}

		public virtual Coroutine TriggerLocalScale( Transform target, Vector3 targetScale )
		{
			PushParameter( "toLocalScale", targetScale );
			PushParameter( "targetTransform", target );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			// pull parameters
			Transform target = HasParameter("targetTransform") ?
				(Transform)PullParameter("targetTransform") : targetTransform;
			if( targetTransform == null )
				yield return new Yield.ErrorBreak("Target Transform not set");
			Vector3 targetScale = HasParameter("toLocalScale") ?
				(Vector3)PullParameter("toLocalScale") : toLocalScale;
			
			if( deltaType != EUnitDeltaSetting.immediate )
			{
				currentScale = target.localScale;
				Vector3 unitMovement = targetScale - currentScale;
				float toBeAdjustedBy = unitMovement.magnitude;
				if( Mathf.Approximately(toBeAdjustedBy,0) )
					yield return Yield.completed;

				unitMovement.Normalize();
				float beenMovedBy = 0;

				while( true )
				{
					// scale closer by delta
					Vector3 jump = unitMovement * delta;
					beenMovedBy += jump.magnitude;

					// when gets more then toBeAdjustedBy break
					if( beenMovedBy > toBeAdjustedBy )
						break;

					currentScale += jump;
					target.localScale = currentScale;
					yield return null;
				}
			}

			target.localScale = targetScale;
		}
	}
}
