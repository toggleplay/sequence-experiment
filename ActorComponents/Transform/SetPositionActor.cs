
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Transform/Set Position")]
	public class SetPositionActor : DeltaActor
	{
		public Transform targetTransform = null;
		// TODO make toPosition a Vector3Value
		public Vector3 toPosition = Vector3.zero;
		Vector3 currentPosition = Vector3.zero;

		protected virtual void Reset()
		{
			if( targetTransform == null )
				targetTransform = this.transform;
		}

		public virtual SetPositionActor Init( Transform target, Vector3 targetPosition )
		{
			targetTransform = target;
			toPosition = targetPosition;
			return this;
		}

		public virtual SetPositionActor Init( Transform target, Vector3 targetPosition, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			targetTransform = target;
			toPosition = targetPosition;
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}

		public virtual Coroutine TriggerPosition( Vector3 targetPosition )
		{
			PushParameter( "toPosition", targetPosition );
			return RunAsCoroutine();
		}
		
		public virtual Coroutine TriggerPosition( Transform target, Vector3 targetPosition )
		{
			PushParameter( "toPosition", targetPosition );
			PushParameter( "targetTransform", target );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			// pull parameters
			Transform target = HasParameter("targetTransform") ?
				(Transform)PullParameter("targetTransform") : targetTransform;
			if( targetTransform == null )
				yield return new Yield.ErrorBreak("Target Transform not set");
			Vector3 targetPosition = HasParameter("toPosition") ?
				(Vector3)PullParameter("toPosition") : toPosition;

			if( deltaType != EUnitDeltaSetting.immediate )
			{
				currentPosition = target.position;
				Vector3 unitMovement = targetPosition - currentPosition;
				float toBeMovedBy = unitMovement.magnitude;
				if( Mathf.Approximately(toBeMovedBy,0) )
					yield return Yield.completed;

				unitMovement.Normalize();
				float beenMovedBy = 0;

				while( true )
				{
					// move closer by delta
					Vector3 jump = unitMovement * delta;
					beenMovedBy += jump.magnitude;

					// when gets more then toBeMovedBy break
					if( beenMovedBy > toBeMovedBy )
						break;

					currentPosition += jump;
					target.position = currentPosition;
					yield return null;
				}
			}

			target.position = targetPosition;
		}
	}
}
