
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Serializables;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Transform/Reset")]
	public class ResetTransformActor : Actor
	{
		public Transform targetTransform = null;
		public TransformBoolean resetValuesOf = new TransformBoolean();
		public bool localSpace = true;

		protected virtual void Reset()
		{
			if( targetTransform == null )
				targetTransform = this.transform;
		}

		public virtual ResetTransformActor Init( Transform target )
		{
			targetTransform = target;
			return this;
		}
		public virtual ResetTransformActor Init( Transform target, TransformBoolean ofValues )
		{
			targetTransform = target;
			resetValuesOf = ofValues;
			
			return this;
		}

		public virtual Coroutine TriggerFaceTransform( Transform target )
		{
			PushParameter( "targetTransform", target );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerFaceTransform( Transform target, TransformBoolean ofValues )
		{
			PushParameter( "targetTransform", target );
			PushParameter( "resetValuesOf", ofValues );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			Transform target = HasParameter("targetTransform") ?
				(Transform)PullParameter("targetTransform") : targetTransform;
			if( target == null )
				yield return new Yield.ErrorBreak("No target to reset transforms for");
			TransformBoolean ofValues = HasParameter("resetValuesOf") ?
				(TransformBoolean)PullParameter("resetValuesOf") : resetValuesOf;

			if( ofValues.position )
			{
				if( localSpace )
					target.localPosition = Vector3.zero;
				else
					target.position = Vector3.zero;
			}
			if( ofValues.rotation )
			{
				if( localSpace )
					target.localRotation = Quaternion.identity;
				else
					target.rotation = Quaternion.identity;
			}
			if( ofValues.scale )
			{
				if( localSpace )
					target.localScale = Vector3.one;
				else
					Debug.LogWarning("Cannot set non-local scale");
			}
		}
	}
}
