
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Transform/Set Anchor Position")]
	public class SetAnchorPositionActor : DeltaActor
	{
		public RectTransform targetTransform = null;

		public Vector2 toAnchorPosition = Vector2.zero;
		Vector2 currentPosition = Vector2.zero;

		protected virtual void Reset()
		{
			if( targetTransform == null )
				targetTransform = this.GetComponent<RectTransform>();
		}


		public virtual SetAnchorPositionActor Init( RectTransform target, Vector2 targetPosition )
		{
			targetTransform = target;
			toAnchorPosition = targetPosition;
			return this;
		}

		public virtual SetAnchorPositionActor Init( RectTransform target, Vector2 targetPosition, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			targetTransform = target;
			toAnchorPosition = targetPosition;
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}

		public virtual Coroutine TriggerLocalPosition( Vector2 targetPosition )
		{
			PushParameter( "toRectPosition", targetPosition );
			return RunAsCoroutine();
		}

		public virtual Coroutine TriggerLocalPosition( Transform target, Vector2 targetPosition )
		{
			PushParameter( "toRectPosition", targetPosition );
			PushParameter( "targetTransform", target );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			// pull parameters
			RectTransform target = HasParameter("targetRectTransform") ?
				(RectTransform)PullParameter("targetRectTransform") : targetTransform;
			if( targetTransform == null )
				yield return new Yield.ErrorBreak("Target RectTransform not set");
			Vector2 targetPosition = HasParameter("toLocalPosition") ?
				(Vector2)PullParameter("toLocalPosition") : toAnchorPosition;

			if( deltaType != EUnitDeltaSetting.immediate )
			{
				currentPosition = target.anchoredPosition;
				Vector2 unitMovement = targetPosition - currentPosition;
				float toBeMovedBy = unitMovement.magnitude;
				unitMovement.Normalize();
				float beenMovedBy = 0;
				if( Mathf.Approximately(toBeMovedBy,0) )
					yield return Yield.completed;

				Vector2 jump = Vector2.zero;

				while( true )
				{
					// move closer by delta
					jump = unitMovement * delta;
					beenMovedBy += jump.magnitude;

					// when gets more then toBeMovedBy break
					if( beenMovedBy > toBeMovedBy )
						break;

					currentPosition += jump;
					target.anchoredPosition = currentPosition;
					yield return null;
				}
			}

			target.anchoredPosition = targetPosition;
		}
	}
}
