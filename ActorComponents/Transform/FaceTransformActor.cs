
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Transform/Face Other")]
	public class FaceTransformActor : Actor
	{
		public Transform facingTransform = null;
		public Transform atTransform = null;
		public Vector3 offsetRotation = Vector3.zero;

		protected virtual void Reset()
		{
			if( facingTransform == null )
				facingTransform = this.transform;
		}

		public virtual FaceTransformActor Init( Transform transformToFace )
		{
			if( facingTransform == null )
				facingTransform = this.transform;

			atTransform = transformToFace;
			offsetRotation = Vector3.zero;

			return this;
		}
		public virtual FaceTransformActor Init( Transform transformToAdjust, Transform transformToFace )
		{
			facingTransform = transformToAdjust;
			atTransform = transformToFace;
			offsetRotation = Vector3.zero;
			
			return this;
		}
		public virtual FaceTransformActor Init( Transform transformToAdjust, Transform transformToFace, Vector3 addRotation )
		{
			facingTransform = transformToAdjust;
			atTransform = transformToFace;
			offsetRotation = addRotation;
			
			return this;
		}

		public virtual Coroutine TriggerFaceTransform( Transform transformToFace )
		{
			PushParameter( "atTransform", transformToFace );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerFaceTransform( Transform transformToFace, Vector3 addRotation )
		{
			PushParameter( "atTransform", transformToFace );
			PushParameter( "offsetRotation", addRotation );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerFaceTransform( Transform transformToAdjust, Transform transformToFace, Vector3 addRotation )
		{
			PushParameter( "facingTransform", transformToAdjust );
			PushParameter( "atTransform", transformToFace );
			PushParameter( "offsetRotation", addRotation );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			Transform faceTransform = HasParameter("facingTransform") ?
				(Transform)PullParameter("facingTransform") : facingTransform;
			if( faceTransform == null )
				yield return new Yield.ErrorBreak("No target to set LookAt on");
			Transform faceAtTransform = HasParameter("atTransform") ?
				(Transform)PullParameter("atTransform") : atTransform;
			if( faceAtTransform == null )
				yield return new Yield.ErrorBreak("No target to look at");
			Vector3 offset = HasParameter("offsetRotation") ?
				(Vector3)PullParameter("offsetRotation") : offsetRotation;

			faceTransform.LookAt( faceAtTransform );
			faceTransform.Rotate( offset );
		}
	}
}
