
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Other/Set TimeScale")]
	public class SetTimeScaleActor : DeltaActor
	{
		public FloatValue toTimeScale = new FloatValue(1);
		float currentValue = 0;

		public virtual SetTimeScaleActor Init( float setToTimeScale )
		{
			toTimeScale = new FloatValue(setToTimeScale);
			return this;
		}

		public virtual SetTimeScaleActor Init( float setToTimeScale, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			toTimeScale = new FloatValue(setToTimeScale);
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}

		public virtual Coroutine TriggerSetTimeScaleActor()
		{
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerSetTimeScale( float scale )
		{
			PushParameter( "toTimeScale", scale );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			// Pull parameters
			float timeScaleValue = 0;
			try   { timeScaleValue = PullParameter<float>("toTimeScale"); }
			catch { timeScaleValue = toTimeScale.Value; }

			if( deltaType != EUnitDeltaSetting.immediate )
			{
				currentValue = Time.timeScale;

				if( timeScaleValue > currentValue )
				{
					while( true )
					{
						currentValue += delta;
						if( currentValue > timeScaleValue )
						{
							break;
						}
						else
						{
							Time.timeScale = currentValue;
							yield return null;
						}
					}
				}
				else
				{
					while( true )
					{
						currentValue -= delta;
						if( currentValue < timeScaleValue )
						{
							break;
						}
						else
						{
							Time.timeScale = currentValue;
							yield return null;
						}
					}
				}
			}

			Time.timeScale = timeScaleValue;
		}
	}
}
