
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Animation/Play Animation")]
	public class PlayAnimationActor : Actor
	{
		public enum EAnimationType { play = 0, playQueued, crossFade, crossFadeQueued };

		public Animation targetAnim = null;
		public string animToPlay = "";
		public bool stopFirst = false;
		public EAnimationType animationForm = EAnimationType.play;

		virtual protected void Reset()
		{
			if( targetAnim == null )
				targetAnim = this.GetComponent<Animation>();
		}

		public virtual PlayAnimationActor Init( Animation target, string toPlay )
		{
			targetAnim = target;
			animToPlay = toPlay;
			return this;
		}
		public virtual PlayAnimationActor Init( Animation target, string toPlay, bool preStop, EAnimationType playType )
		{
			targetAnim = target;
			animToPlay = toPlay;
			stopFirst = preStop;
			animationForm = playType;
			return this;
		}

		public virtual Coroutine TriggerPlayAnimation( string toPlay )
		{
			PushParameter( "animToPlay", toPlay );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerPlayAnimation( Animation target, string toPlay )
		{
			PushParameter( "targetAnim", target );
			PushParameter( "animToPlay", toPlay );
			return RunAsCoroutine();
		}

		public virtual Coroutine TriggerPlayAnimation( Animation target, string toPlay, EAnimationType playType )
		{
			PushParameter( "animToPlay", toPlay );
			PushParameter( "targetAnim", target );
			PushParameter( "animationForm", playType );
			return RunAsCoroutine();
		}


		protected override IEnumerator PerformActionRoutine()
		{
			// get correct values
			Animation target = null;
			try   { target = PullParameter<Animation>("targetAnim"); }
			catch { target = targetAnim; }

			if( target == null )
				yield return new Yield.ErrorBreak("Actor Animation reference not set");

			string animationClipToPlay = null;
			EAnimationType playType = EAnimationType.play;

			try   { animationClipToPlay = PullParameter<string>("animToPlay"); }
			catch { animationClipToPlay = animToPlay; }
			try   { playType = PullParameter<EAnimationType>("animationForm"); }
			catch { playType = animationForm; }


		
			if( animationClipToPlay != "" && target != null )
			{
				if( stopFirst )
					target.Stop();
				
				switch(playType)
				{
				case EAnimationType.play:
					target.Play(animationClipToPlay);
					break;
				case EAnimationType.playQueued:
					target.PlayQueued(animationClipToPlay);
					break;
				case EAnimationType.crossFade:
					target.CrossFade(animationClipToPlay);
					break;
				case EAnimationType.crossFadeQueued:
					target.CrossFadeQueued(animationClipToPlay);
					break;
				}
			}

			while( target.isPlaying == true )
				yield return null;
		}
	}
}
