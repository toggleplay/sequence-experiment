
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Animation/Stop Animation")]
	public class StopAnimationActor : DeltaActor
	{
		public Animation targetAnim = null;
		public string animToStop = "";

		virtual protected void Reset()
		{
			if( targetAnim == null )
				targetAnim = this.GetComponent<Animation>();
		}

		public virtual StopAnimationActor Init( string toStop )
		{
			if( targetAnim == null )
				targetAnim = this.GetComponent<Animation>();
			animToStop = toStop;
			return this;
		}

		public virtual StopAnimationActor Init( Animation target, string toStop )
		{
			targetAnim = target;
			animToStop = toStop;
			return this;
		}

		public virtual Coroutine TriggerStopAnimation( string toStop )
		{
			PushParameter( "animToStop", toStop );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerStopAnimation( Animation stopAnimation, string toStop )
		{
			PushParameter( "targetAnim", stopAnimation );
			PushParameter( "animToStop", toStop );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			Animation anim = null;
			try   { anim = PullParameter<Animation>("targetAnim"); }
			catch { anim = targetAnim; }
			if( anim == null )
				yield return new Yield.ErrorBreak("Actor Animation reference not set");
			
			string stopClip = null;
			try   { stopClip = PullParameter<string>("animToStop"); }
			catch { stopClip = animToStop; }
		
			if( deltaType == EUnitDeltaSetting.immediate )
			{
				if( stopClip == "" )
					anim.Stop();
				else
					anim.Stop(stopClip);
			}
			else if( stopClip == "" )
			{
				yield return new Yield.ErrorBreak("No clip name to stop, must define clip if slowing down over time");
			}
			else
			{
				if( anim.GetClip(stopClip) == null )
					yield return new Yield.ErrorBreak("Animation does not have clip : " + stopClip );
				else if( anim.IsPlaying(stopClip) == true )
				{
					if( anim[stopClip].speed > 0 )
					{
						while( anim[stopClip].speed > 0 )
						{
							anim[stopClip].speed -= delta;
							yield return null;
						}
					}
					else
					{
						while( anim[stopClip].speed < 0 )
						{
							anim[stopClip].speed += delta;
							yield return null;
						}
					}

					anim[stopClip].speed = 0;
				}
			}
		}
	}
}
