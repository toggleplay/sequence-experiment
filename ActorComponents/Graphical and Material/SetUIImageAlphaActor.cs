
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

using UnityEngine.UI;

namespace TogglePlay.Actors
{

	[AddComponentMenu("Toggle Play/Actors/Graphical/Set UIImage Alpha")]
	public class SetUIImageAlphaActor : DeltaActor
	{
		public Image targetImage = null;
		public FloatValue toAlpha = new FloatValue(1);
		float currentAlpha = 0;
		[HideInInspector, FeedOutput] public float activeAlpha = 0;

		protected virtual void Reset()
		{
			if( targetImage == null )
				targetImage = gameObject.GetComponent<Image>();
		}

		public virtual SetUIImageAlphaActor Init( Image target, float alpha )
		{
			targetImage = target;
			toAlpha = new FloatValue(alpha);
			return this;
		}

		public virtual SetUIImageAlphaActor Init( Image target, float alpha, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			targetImage = target;
			toAlpha = new FloatValue(alpha);
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}
		
		public virtual Coroutine TriggerSetAlpha( float alpha )
		{
			PushParameter( "toAlpha", alpha );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerSetAlpha( Image target, float alpha )
		{
			PushParameter( "targetImage", target );
			PushParameter( "toAlpha", alpha );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			// Pull alpha parameter if it has been set
			Image target = null;
			float toAlphaValue = 0;

			try   { target = PullParameter<Image>("targetImage"); }
			catch { target = targetImage; }
			if( target == null )
				yield return new Yield.ErrorBreak("Target Image not set");

			try   { toAlphaValue = PullParameter<float>("toAlpha"); }
			catch { toAlphaValue = toAlpha.Value; }

			if( deltaType != EUnitDeltaSetting.immediate )
			{
				currentAlpha = targetImage.material.color.a;

				if( toAlphaValue > currentAlpha )
				{
					while( true )
					{
						currentAlpha += delta;
						if( currentAlpha > toAlphaValue )
						{
							break;
						}
						else
						{
							Color increasedA = target.color;
							increasedA.a = currentAlpha;
							target.color = increasedA;
							yield return null;
						}
					}
				}
				else
				{
					while( true )
					{
						currentAlpha -= delta;
						if( currentAlpha < toAlphaValue )
						{
							break;
						}
						else
						{
							Color decreasedA = target.color;
							decreasedA.a = currentAlpha;
							target.color = decreasedA;
							yield return null;
						}
					}
				}
			}

			Color tempColor = target.color;
			tempColor.a = toAlphaValue;
			target.color = tempColor;
			target.SetAllDirty();
		}
	}
}
