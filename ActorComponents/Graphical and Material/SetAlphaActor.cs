
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	public class Feedable : Attribute
	{
	}
	public class FeedOutput : Attribute
	{
	}

	[AddComponentMenu("Toggle Play/Actors/Graphical/Set Alpha")]
	public class SetAlphaActor : DeltaActor
	{
		public Renderer targetRenderer = null;
		public FloatValue toAlpha = new FloatValue(1);
		float currentAlpha = 0;
		[HideInInspector, FeedOutput] public float activeAlpha = 0;

		protected virtual void Reset()
		{
			if( targetRenderer == null )
				targetRenderer = this.GetComponent<Renderer>();
		}

		public virtual SetAlphaActor Init( Renderer target, float alpha )
		{
			targetRenderer = target;
			toAlpha = new FloatValue(alpha);
			return this;
		}

		public virtual SetAlphaActor Init( Renderer target, float alpha, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			targetRenderer = target;
			toAlpha = new FloatValue(alpha);
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}
		
		public virtual Coroutine TriggerSetAlpha( float alpha )
		{
			PushParameter( "toAlpha", alpha );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerSetAlpha( Renderer target, float alpha )
		{
			PushParameter( "targetRenderer", target );
			PushParameter( "toAlpha", alpha );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			// Pull alpha parameter if it has been set
			Renderer target = null;
			float toAlphaValue = 0;

			try   { target = PullParameter<Renderer>("targetRenderer"); }
			catch { target = targetRenderer; }
			if( target == null )
				yield return new Yield.ErrorBreak("Target Renderer not set");

			try   { toAlphaValue = PullParameter<float>("toAlpha"); }
			catch { toAlphaValue = toAlpha.Value; }

			if( deltaType != EUnitDeltaSetting.immediate )
			{
				currentAlpha = targetRenderer.material.color.a;

				if( toAlphaValue > currentAlpha )
				{
					while( true )
					{
						currentAlpha += delta;
						if( currentAlpha > toAlphaValue )
						{
							break;
						}
						else
						{
							Color increasedA = target.material.color;
							increasedA.a = currentAlpha;
							target.material.color = increasedA;
							yield return null;
						}
					}
				}
				else
				{
					while( true )
					{
						currentAlpha -= delta;
						if( currentAlpha < toAlphaValue )
						{
							break;
						}
						else
						{
							Color decreasedA = target.material.color;
							decreasedA.a = currentAlpha;
							target.material.color = decreasedA;
							yield return null;
						}
					}
				}
			}

			Color tempColor = target.material.color;
			tempColor.a = toAlphaValue;
			target.material.color = tempColor;
		}
	}
}
