
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	[AddComponentMenu("Toggle Play/Actors/Graphical/Set Color")]
	public class SetMaterialColorActor : DeltaActor
	{
		public Renderer targetRenderer = null;
		public Color toColor = Color.white;
		Color currentColor = Color.white;

		protected virtual void Reset()
		{
			if( targetRenderer == null )
				targetRenderer = this.GetComponent<Renderer>();
		}

		public virtual SetMaterialColorActor Init( Renderer target, Color targetColor )
		{
			targetRenderer = target;
			toColor = targetColor;
			return this;
		}

		public virtual SetMaterialColorActor Init( Renderer target, Color targetColor, EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			targetRenderer = target;
			toColor = targetColor;
			SetDeltaType( deltaTypeSetting, deltaTypeValue );
			return this;
		}
		
		public virtual Coroutine TriggerSetAlpha( Color targetColor )
		{
			PushParameter( "toColor", targetColor );
			return RunAsCoroutine();
		}
		public virtual Coroutine TriggerSetAlpha( Renderer target, Color targetColor )
		{
			PushParameter( "targetRenderer", target );
			PushParameter( "toColor", targetColor );
			return RunAsCoroutine();
		}

		protected override IEnumerator PerformActionRoutine()
		{
			// Pull alpha parameter if it has been set
			Renderer target = null;
			Color targetColor = Color.white;

			try   { target = PullParameter<Renderer>("targetRenderer"); }
			catch { target = targetRenderer; }
			if( target == null )
				yield return new Yield.ErrorBreak("Target Renderer not set");

			try   { targetColor = PullParameter<Color>("toColor"); }
			catch { targetColor = toColor; }

			if( deltaType != EUnitDeltaSetting.immediate )
			{
				currentColor = targetRenderer.material.color;

				Vector3 currentColVec3 = new Vector3(currentColor.r,currentColor.g,currentColor.b);
				Vector3 targetColVec3 = new Vector3(targetColor.r, targetColor.g, targetColor.b);
				Vector3 unitMovement = targetColVec3 - currentColVec3;
				float toBeMovedBy = unitMovement.magnitude;

				if( Mathf.Approximately(toBeMovedBy,0) )
					yield return Yield.completed;

				unitMovement.Normalize();

				float beenMovedBy = 0;
				Vector3 jump = Vector3.zero;
				
				while( true )
				{
					// move closer by delta
					jump = unitMovement * delta;
					beenMovedBy += jump.magnitude;
					
					// when gets more then toBeMovedBy break
					if( beenMovedBy > toBeMovedBy )
						break;
					
					currentColVec3 += jump;

					target.material.color = new Color(currentColVec3.x,currentColVec3.y, currentColVec3.z);

					yield return null;
				}
			}

			target.material.color = targetColor;
		}
	}
}
