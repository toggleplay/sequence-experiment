﻿using UnityEngine;
using UnityEditor;

using TogglePlay.Actors;

namespace InspectorEditor.Core
{

	[CustomEditor(typeof(DeltaActor),true), CanEditMultipleObjects]
	public class DeltaActorEditor : Editor
	{
		SerializedProperty timePerUnit;
		SerializedProperty deltaType;
		SerializedProperty unitsPerSecond;
		SerializedProperty useTrueDelta;
		
		public void OnEnable()
		{
			timePerUnit = serializedObject.FindProperty("timePerUnit");
			unitsPerSecond = serializedObject.FindProperty("unitsPerSecond");
			deltaType = serializedObject.FindProperty("deltaType");
			useTrueDelta = serializedObject.FindProperty("useTrueDelta");
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			serializedObject.Update();

			EditorGUILayout.PropertyField( deltaType );


			switch( (DeltaActor.EUnitDeltaSetting)deltaType.enumValueIndex )
			{
			case DeltaActor.EUnitDeltaSetting.immediate:
				break;
			case DeltaActor.EUnitDeltaSetting.timePerUnit:
				EditorGUILayout.PropertyField( useTrueDelta );
				EditorGUILayout.PropertyField( timePerUnit );
				break;
			case DeltaActor.EUnitDeltaSetting.unitsPerSecond:
				EditorGUILayout.PropertyField( useTrueDelta );
				EditorGUILayout.PropertyField( unitsPerSecond );
				break;
			}

			serializedObject.ApplyModifiedProperties();
		}

		void OnInspectorUpdate()
		{
			Repaint();
		}
	}
}
