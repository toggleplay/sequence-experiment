
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using TogglePlay.Delegates;
using TogglePlay.Generics;

namespace TogglePlay.Actions
{
	public abstract class Action : ParametersObject
	{
		public override Coroutine RunAsCoroutine()
		{
			RunAction();
			return null;
		}

		public override Coroutine RunAsCoroutine( VoidDelegate endedCallback )
		{
			RunAction();
			return null;
		}

		public override IEnumerator RunAsEnumerator()
		{
			RunAction();
			yield break;
		}

		public override IEnumerator RunAsEnumerator( VoidDelegate endedCallback )
		{
			RunAction();
			yield break;
		}

		protected abstract void RunAction();
	}
}