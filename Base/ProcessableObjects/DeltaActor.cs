
using UnityEngine;
using System;
using System.Collections;
using TogglePlay.Delegates;
using TogglePlay.Values;

namespace TogglePlay.Actors
{
	public abstract class DeltaActor : Actor
	{
		public enum EUnitDeltaSetting { immediate = 0, timePerUnit, unitsPerSecond };


		// a setting with individual values, making it easier to understand
		[HideInInspector] public EUnitDeltaSetting deltaType = EUnitDeltaSetting.unitsPerSecond;

		[HideInInspector][SerializeField] protected FloatValue timePerUnit = new FloatValue(1);			// time it takes to increase by 1
		[HideInInspector][SerializeField] protected FloatValue unitsPerSecond = new FloatValue(1);		// magnitudes per second

		[HideInInspector] public bool useTrueDelta = false;
		[HideInInspector] public float speed = 1;

		private float previousRealtime = -1f;
		private float activeDelta = -1f;

#if UNITY_EDITOR
		private float magnitude
		{
			get
			{
				if( deltaType == EUnitDeltaSetting.timePerUnit )
				{
					if( Mathf.Approximately( timePerUnit.Value, 0 ) )
					{
						Debug.LogWarning("TimePerUnit set at 0, cannot devide by 0");
						return 0f;
					}
					return (1f / timePerUnit.Value); // convert to units per second
				}
				else
					return unitsPerSecond.Value;
			}
		}
#else
		private float magnitude = 0;
#endif

		protected virtual void Awake()
		{
#if ! UNITY_EDITOR
			if( deltaType == EUnitDeltaSetting.timePerUnit )
				magnitude = (1f / timePerUnit.Value); // convert to units per second
			else
				magnitude = unitsPerSecond.Value;
#endif
		}

		protected virtual void OnDisable()
		{
			// stop it from include time when disabled
			previousRealtime = -1f;
		}

		protected void SetDeltaType( EUnitDeltaSetting deltaTypeSetting, float deltaTypeValue )
		{
			deltaType = deltaTypeSetting;
			if( deltaType == EUnitDeltaSetting.timePerUnit )
				timePerUnit.Set( deltaTypeValue );
			else if( deltaType == EUnitDeltaSetting.unitsPerSecond )
				unitsPerSecond.Set( deltaTypeValue );
			
#if ! UNITY_EDITOR
			if( deltaType == EUnitDeltaSetting.timePerUnit )
				magnitude = (1f / timePerUnit.Value); // convert to units per second
			else
				magnitude = unitsPerSecond.Value;
#endif
		}

		protected float delta
		{
			get
			{
				if( ! useTrueDelta )
				{
					return Time.deltaTime * magnitude * speed;
				}
				else
				{
					if( Mathf.Approximately( previousRealtime, Time.realtimeSinceStartup ) )
						return activeDelta * speed;
					if( previousRealtime < 0 )
					{
						previousRealtime = Time.realtimeSinceStartup;
						return 0;
					}

					activeDelta = (Time.realtimeSinceStartup - previousRealtime) * magnitude;
					previousRealtime = Time.realtimeSinceStartup;
					return activeDelta * speed;
				}
			}
		}
	}
}