
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using TogglePlay.Delegates;
using TogglePlay.Generics;

namespace TogglePlay
{
	// TODO feeders
	public abstract class ParametersObject : ProcessableObject
	{
		//private KeyedStack<object> parameterStack = new KeyedStack<object>();

		private Dictionary<string, Stack<object>> parameters = new Dictionary<string, Stack<object>>();

		protected bool HasParameter( string key )
		{
			return parameters.ContainsKey(key);
		}
		protected void PushParameter( string key, object element )
		{
			if( ! parameters.ContainsKey(key) )
				parameters.Add(key, new Stack<object>());
			parameters[key].Push(element);
		}
		protected object PullParameter( string key )
		{
			// hiding parameterStack because pop and peek options here may be a future addon
			if( ! parameters.ContainsKey(key) )
				throw new KeyNotFoundException(key);
			else
			{
				object rtnObj = parameters[key].Pop();
				if( (System.Object)rtnObj == null )
					throw new NullReferenceException(key);
				else
					return rtnObj;
			}
		}
		protected T PullParameter<T>( string key )
		{
			// hiding parameterStack because pop and peek options here may be a future addon
			if( ! parameters.ContainsKey(key) )
				throw new KeyNotFoundException(key);
			else
			{
				object rtnObj = parameters[key].Pop();
				if( (System.Object)rtnObj == null )
					throw new NullReferenceException(key);
				else
				{
					T casted = (T)rtnObj;
					if( (System.Object)casted == null )
						throw new InvalidCastException(key);
					else
						return casted;
				}
			}
		}

//		protected bool HasParameter( string key )
//		{
//			if( parameterStack == null )
//				parameterStack = new KeyedStack<object>();
//
//			return parameterStack.ContainsKey(key);
//		}
//		protected void PushParameter( string key, object element )
//		{
//			if( parameterStack == null )
//				parameterStack = new KeyedStack<object>();
//
//			parameterStack.Push(key,element);
//		}
//		protected object PullParameter( string key )
//		{
//			if( parameterStack == null )
//				parameterStack = new KeyedStack<object>();
//
//			// hiding parameterStack because pop and peek options here may be a future addon
//			if( ! parameterStack.ContainsKey(key) )
//				throw new KeyNotFoundException(key);
//			else
//			{
//				object rtnObj = parameterStack.Pop(key);
//				if( (System.Object)rtnObj == null )
//					throw new NullReferenceException(key);
//				else
//					return rtnObj;
//			}
//		}
//		protected T PullParameter<T>( string key )
//		{
//			if( parameterStack == null )
//				parameterStack = new KeyedStack<object>();
//
//			// hiding parameterStack because pop and peek options here may be a future addon
//			if( ! parameterStack.ContainsKey(key) )
//				throw new KeyNotFoundException(key);
//			else
//			{
//				object rtnObj = parameterStack.Pop(key);
//				if( (System.Object)rtnObj == null )
//					throw new NullReferenceException(key);
//				else
//				{
//					T casted = (T)rtnObj;
//					if( (System.Object)casted == null )
//						throw new InvalidCastException(key);
//					else
//						return casted;
//				}
//			}
//		}
	}
}

