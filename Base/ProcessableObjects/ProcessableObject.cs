
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using TogglePlay.Delegates;
using TogglePlay.Generics;

namespace TogglePlay
{
	public abstract class ProcessableObject : MonoBehaviour
	{
		public abstract Coroutine RunAsCoroutine();
		public abstract Coroutine RunAsCoroutine( VoidDelegate endedCallback );
		public abstract IEnumerator RunAsEnumerator();
		public abstract IEnumerator RunAsEnumerator( VoidDelegate endedCallback );
	}
}

