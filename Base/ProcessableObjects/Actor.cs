
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using TogglePlay.Delegates;
using TogglePlay.Generics;

namespace TogglePlay.Actors
{
	public abstract class Actor : ParametersObject
	{
		public enum EPerformOnFinishStyle { yieldPerformSequencially = 0, performSequencially, yieldPerform, perform };
		public enum EActorState { begin = 0, actorFinished, error, cancelled, continueBroken, noState }
		public delegate void ActorStateDelegate( EActorState state );

		public ActorStateDelegate OnActorState = null;

		bool toCancelRoutine = false;

		bool inRoutine = false;
		public bool isInRoutine { get{ return inRoutine; } }
		bool paused = false;
		public bool isPaused { get{ return paused; } }

		public List<Actor> performOnFinish = new List<Actor>();
		public EPerformOnFinishStyle performOnFinishType = EPerformOnFinishStyle.yieldPerformSequencially;

		//-------------------------------------------------------------------------------------------------------

		public virtual void Pause()
		{
			paused = true;
		}
		public virtual void Resume()
		{
			paused = false;
		}


		public override Coroutine RunAsCoroutine()
		{
			return StartCoroutine( InternalRoutine(null) );
		}
		public override IEnumerator RunAsEnumerator()
		{
			return InternalRoutine(null);
		}
		public Coroutine RunAsCoroutine( List<KeyValuePair<string,object>> parameters )
		{
			if( parameters != null )
			{
				foreach( KeyValuePair<string,object> kvp in parameters )
					PushParameter(kvp.Key, kvp.Value);
			}
			
			return StartCoroutine( InternalRoutine(null) );
		}

		public override Coroutine RunAsCoroutine( VoidDelegate endedCallback )
		{
			return RunAsCoroutine( endedCallback, null );
		}
		public override IEnumerator RunAsEnumerator(VoidDelegate endedCallback)
		{
			if( endedCallback == null )
				return InternalRoutine(null);
			return InternalRoutine( delegate(EActorState s)
			{
				endedCallback();
			});
		}
		public Coroutine RunAsCoroutine( VoidDelegate endedCallback, List<KeyValuePair<string,object>> parameters )
		{
			if( parameters != null )
			{
				foreach( KeyValuePair<string,object> kvp in parameters )
					PushParameter(kvp.Key, kvp.Value);
			}

			if( endedCallback != null )
			{
				return StartCoroutine( InternalRoutine( delegate( EActorState s )
				{
					if( s != EActorState.begin )
						endedCallback();
				} ) );
			}
			else
			{
				return StartCoroutine( InternalRoutine(null) );
			}
		}

		public Coroutine RunAsCoroutine( ActorStateDelegate stateCallback )
		{
			return StartCoroutine( InternalRoutine(stateCallback) );
		}
		public Coroutine RunAsCoroutine( ActorStateDelegate stateCallback, List<KeyValuePair<string,object>> parameters )
		{
			if( parameters != null )
			{
				foreach( KeyValuePair<string,object> kvp in parameters )
					PushParameter(kvp.Key, kvp.Value);
			}

			return StartCoroutine( InternalRoutine(stateCallback) );
		}

		//------------------------------------------------------------------------------------------------------------

		private IEnumerator InternalRoutine( ActorStateDelegate callback )
		{
			toCancelRoutine = false;
			inRoutine = true;

			StateCallback( EActorState.begin, callback );

			Type errorType = typeof( Yield.ErrorBreak );
			Type completeType = typeof( Yield.CompleteBreak );
			Type cancelType = typeof( Yield.CancelBreak );
			Type continueType = typeof( Yield.ContinueBreak );
			Type inst = null;

			while( paused )
				yield return null;

			EActorState toCallback = EActorState.noState;

			IEnumerator enumInstruc = PerformActionRoutine();
			while( enumInstruc.MoveNext() )
			{
				if( toCancelRoutine )
				{
					toCallback = EActorState.cancelled;
					break;
				}

				YieldInstruction instruction = (YieldInstruction)enumInstruc.Current;
				if( instruction != null )
				{
					inst = instruction.GetType();
					if( inst == errorType )
					{
						toCallback = EActorState.error;
						Debug.LogError( ((Yield.ErrorBreak)instruction).errorMessage, this );
						break;
					}
					else if( inst == completeType )
					{
						// irrelevent most cases, but keeps things clean and readable
						toCallback = EActorState.actorFinished;
						break;
					}
					else if( inst == cancelType )
					{
						toCallback = EActorState.cancelled;
						break;
					}
					else if( inst == continueType )
					{
						toCallback = EActorState.continueBroken;
						// start a coroutine to finish off the sequence
						StartCoroutine( enumInstruc );
						break;
					}
				}

				yield return instruction;
				while( paused )
					yield return null;
			}

			// routine finished, inform callbacks on the finish type
			inRoutine = false;

			StateCallback( toCallback, callback );

			// perform actors to happen post this actor, (All are the routine action, so no events will be called)
			switch( performOnFinishType )
			{
			case EPerformOnFinishStyle.yieldPerformSequencially:
				// go through each actor, performing its direct routine one by one
				foreach( Actor finActor in performOnFinish )
				{
					enumInstruc = finActor.PerformActionRoutine();
					while( enumInstruc.MoveNext() )
						yield return enumInstruc;
				}
				break;
			case EPerformOnFinishStyle.yieldPerform:
				// local enumerator function that runs through the actors all at once and breaks when all completed
				enumInstruc = PerformOnFinishYielded();
				while( enumInstruc.MoveNext() )
					yield return enumInstruc;
				break;
			case EPerformOnFinishStyle.perform:
				// just tell them all to perform
				foreach( Actor a in performOnFinish )
					a.RunAsCoroutine();
				break;
			case EPerformOnFinishStyle.performSequencially:
				// start a coroutine to sequencially start each actor
				StartCoroutine(PerformOnFinishSequencial());
				break;
			}
		}

		private IEnumerator PerformOnFinishYielded()
		{
			List<IEnumerator> enumerators = new List<IEnumerator>();

			foreach( Actor a in performOnFinish )
			{
				// perform all at once, bypass internal routine, and go directly to the performance routine.
				enumerators.Add( a.PerformActionRoutine() );
			}

			// loop through all actors enumerators until completed
			while( true )
			{
				for( int i = enumerators.Count-1; i >= 0; --i )
				{
					if( enumerators[i].MoveNext() == false )
						enumerators.RemoveAt(i);
				}
				if( enumerators.Count == 0 )
					break;
				yield return null;
			}
		}

		private IEnumerator PerformOnFinishSequencial()
		{
			foreach( Actor a in performOnFinish )
			{
				// perform one by one, bypass internal routine, and go directly to the performance routine.
				IEnumerator enumInstruc =  a.PerformActionRoutine();
				while( enumInstruc.MoveNext() )
					yield return enumInstruc;
			}
		}

		protected void StateCallback( EActorState state, ActorStateDelegate otherCallback )
		{
			if( OnActorState != null )
				OnActorState(state);
			if( otherCallback != null )
				otherCallback(state);
		}

		protected abstract System.Collections.IEnumerator PerformActionRoutine();

		// break the enumerable loop on its next iternation
		public virtual void Cancel()
		{
			if( inRoutine == false )
			{
				// routine has already finished, or has not started
				return;
			}

			toCancelRoutine = true;
		}

		public virtual Coroutine OneShot()
		{
			return StartCoroutine( OneShotRoutine(-1) );
		}
		public virtual Coroutine OneShot(float delay)
		{
			return StartCoroutine( OneShotRoutine(delay) );
		}

		protected virtual IEnumerator OneShotRoutine( float delay )
		{
			if( delay > 0 )
				yield return new WaitForSeconds(delay);

			// perform the process then destroy the component upon the completion callback
			bool fin = false;
			yield return StartCoroutine( InternalRoutine(delegate( EActorState s ) 
			{
				fin = true;
			}) );

			while( ! fin )
			{
				yield return null;
			}

			Destroy(this);
		}
	}
}