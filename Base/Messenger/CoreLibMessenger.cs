using UnityEngine;
using System.Collections.Generic;
using TogglePlay.Processors;

namespace TogglePlay.Messengers
{
	public abstract class CoreLibMessenger : MonoBehaviour
	{
		public List<ProcessableObject> processorObjectss = new List<ProcessableObject>();

		protected virtual void Send()
		{
			foreach( ProcessableObject p in processorObjectss )
			{
				if( p == null )
					continue;
				p.RunAsCoroutine();
			}
		}

		protected virtual void OnValidate()
		{
			// make sure any ProcessOnMessenger's do not include this
			for( int p = processorObjectss.Count-1; p >= 0; --p )
			{
				ProcessableObject procObj = processorObjectss[p];
				if( procObj == null )
					continue;
				if( procObj.GetType() != typeof(ProcessOnMessenger) )
					continue;

				ProcessOnMessenger proMess = procObj as ProcessOnMessenger;
				if( proMess == null )
					continue;

				foreach( EventMessenger m in proMess.messengers )
				{
					if( m == this )
					{
						processorObjectss.RemoveAt(p);
						Debug.LogWarning("Processor already connects to this messenger as a ProcessOnMessage,\n" +
							"cannot create a double message, removing from messenger list");
					}
				}
			}
		}
	}
}
