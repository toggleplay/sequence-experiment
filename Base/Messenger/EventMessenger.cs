﻿using UnityEngine;
using System.Collections.Generic;
using TogglePlay.Processors;

namespace TogglePlay.Messengers
{
	public abstract class EventMessenger : CoreLibMessenger
	{
		public string callerMessage = "OnMessenger";
		public string stringMessage = "";

		public event Delegates.StringDelegate onMessage = null;
		public event Delegates.VoidDelegate onSent = null;

		public GameObject externalObject = null;

		protected override void Send()
		{
			base.Send();

			if( callerMessage != "" )
			{
				if( externalObject == null )
				{
					SendMessage( callerMessage, SendMessageOptions.DontRequireReceiver );
					SendMessage( callerMessage, stringMessage, SendMessageOptions.DontRequireReceiver );
				}
				else
				{
					externalObject.SendMessage( callerMessage, SendMessageOptions.DontRequireReceiver );
					externalObject.SendMessage( callerMessage, stringMessage, SendMessageOptions.DontRequireReceiver );
				}
			}

			if( onMessage != null )
				onMessage(stringMessage);
			if( onSent != null )
				onSent();
		}
	}
}
