using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Start")]
	public class ProcessOnStart : Processor
	{
		void Start()
		{
			RunAsCoroutine();
		}
	}
}