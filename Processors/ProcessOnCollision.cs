using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Collision")]
	public class ProcessOnCollision : Processor
	{
		public string triggerTag = "";
		public enum ETrigger { enter = 0, stay, exit };
		public ETrigger trigger = ETrigger.enter;

		public virtual void OnCollisionEnter( Collision col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.enter )
				RunAsCoroutine();
		}
		public virtual void OnCollisionStay( Collision col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.stay )
				RunAsCoroutine();
		}
		public virtual void OnCollisionExit( Collision col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.exit )
				RunAsCoroutine();
		}
	}
}