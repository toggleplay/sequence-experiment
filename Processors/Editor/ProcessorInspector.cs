using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

using TogglePlay;
using TogglePlay.Processors;
using TogglePlay.Actors;
using TogglePlay.Restrictions;

namespace InspectorEditor.Core
{

	[CustomEditor(typeof(Processor),true), CanEditMultipleObjects]
	public class ProcessorInspector : Editor
	{
		class ObjectToAdd
		{
			public Object obj = null;
			public bool siblinged = false;
			public ObjectToAdd( Object o, bool withSiblings )
			{
				obj = o;
				siblinged = withSiblings;
			}
		}

		SerializedProperty p_defaultList;
		SerializedProperty p_lists;
		SerializedProperty p_stopOnListChange;
		SerializedProperty p_whenInSequence;
		SerializedProperty p_cycleToNextEachTrigger;
		SerializedProperty p_restrictions;
		SerializedProperty p_processLimitation;
		SerializedProperty p_maximumProcessCount;
		public virtual void OnEnable()
		{
			p_defaultList = serializedObject.FindProperty("defaultList");
			p_lists = serializedObject.FindProperty("processableLists");

			p_stopOnListChange = serializedObject.FindProperty("stopOnListChange");
			p_whenInSequence = serializedObject.FindProperty("whenInSequence");

			p_cycleToNextEachTrigger = serializedObject.FindProperty("cycleToNextEachTrigger");

			p_restrictions = serializedObject.FindProperty("restrictions");
			p_processLimitation = serializedObject.FindProperty("processLimitation");
			p_maximumProcessCount = serializedObject.FindProperty("maximumProcessCount");
		}

		static Color bgColH = new Color(0.75f,0.75f,0.76f,1);
		static Color bgColB = new Color(0.84f,0.84f,0.84f);
		static Color conColH = new Color(0.6f,1,1);

		int inlistCountFromInput = -1;
		int lastKnownInlistCount = -1; // last time the count was set directly

		Processor script = null;
		int listSelectedIndex = -1;
		SerializedProperty p_selectedList = null;
		SerializedProperty p_selectedObjects = null;

		int restrictorsCountFromInput = -1;
		int lastKnownRestrictorCount = -1; // last time the count was set directly

		static List<List<Rect>> boxesToRender = new List<List<Rect>>();
		void PushBox( Rect r )
		{
			if( boxesToRender.Count == 0 )
				return;
			Rect n = new Rect(r);
			boxesToRender[boxesToRender.Count-1].Add(n);
		}


		void HeadRect( Rect contentRect )
		{
			contentRect.x -= 4; contentRect.width += 7;
			
			GUI.backgroundColor = bgColH;
			GUI.Box( contentRect, GUIContent.none );
			GUI.backgroundColor = Color.white;

			contentRect.x += 4; contentRect.width -= 7;
		}
		void BodyRect( Rect contentRect, int size )
		{
			contentRect.x -= 3; contentRect.width += 5;
			contentRect.height = 18 * size;
			
			GUI.backgroundColor = bgColB;
			GUI.Box( contentRect, GUIContent.none );
			GUI.backgroundColor = Color.white;

			contentRect.x += 4; contentRect.width -= 7;
		}

		public override void OnInspectorGUI()
		{
			Rect r = new Rect();

			SerializedProperty p_shouldProcessObjectsFoldOut = serializedObject.FindProperty("shouldProcessObjectsFoldOut");
			SerializedProperty p_shouldRestrictorsFoldOut = serializedObject.FindProperty("shouldRestrictorsFoldOut");

			SerializedProperty p_showProcessingLists = serializedObject.FindProperty("showProcessingLists");
			SerializedProperty p_showProcessSettings = serializedObject.FindProperty("showProcessSettings");
			SerializedProperty p_showRestrictionSettings = serializedObject.FindProperty("showRestrictionSettings");


			base.OnInspectorGUI();
			serializedObject.Update();

			if( script == null )
				script = serializedObject.targetObject as Processor;
			List<ProcessableList> processorLists = script.processableLists;
			string nameOfDefault = p_defaultList.stringValue;

			r = EditorGUILayout.GetControlRect();
			HeadRect(r);
			GUI.contentColor = conColH; p_showProcessingLists.boolValue = EditorGUI.ToggleLeft( r, "Processing Lists", p_showProcessingLists.boolValue ); GUI.contentColor = Color.white;
			EditorGUI.indentLevel++;

			if( p_showProcessingLists.boolValue )
			{
				r.y += 16; r.height+=2;
				int listAreaCount = (processorLists.Count > 0 ? 5 : 2);
				if( p_shouldProcessObjectsFoldOut.boolValue )
					listAreaCount += script.GetObjectCountInList(nameOfDefault) + 1;
				BodyRect(r, listAreaCount);

				// get available processableLists and drop down display for default
				string[] strArray = new string[processorLists.Count];
				int defaultListIndex = -1;
				for( int i=0; i<processorLists.Count; ++i )
				{
					if( nameOfDefault == processorLists[i].listName )
						defaultListIndex = i;
					strArray[i] = processorLists[i].listName;
				}

				if( defaultListIndex < 0 )
				{
					if( processorLists.Count == 0 )
					{
						r = EditorGUILayout.GetControlRect();
						EditorGUI.LabelField(r, "No Lists");
					}
					else
					{
						defaultListIndex = 0;
						r = EditorGUILayout.GetControlRect();
						defaultListIndex = EditorGUI.Popup(r, "Default List", defaultListIndex, strArray);
						p_defaultList.stringValue = strArray[defaultListIndex];
					}
				}
				else
				{
					r = EditorGUILayout.GetControlRect();
					defaultListIndex = EditorGUI.Popup(r, "Default List", defaultListIndex, strArray);
					p_defaultList.stringValue = processorLists[defaultListIndex].listName;
				}



				// check if its first selected .'. starting current selected, 
				if( listSelectedIndex < 0 )
				{
					lastKnownRestrictorCount = restrictorsCountFromInput = p_restrictions.arraySize;
					listSelectedIndex = defaultListIndex >= 0 ? defaultListIndex : 0;

					if( listSelectedIndex < processorLists.Count && processorLists.Count > 0 )
					{
						p_selectedList = p_lists.GetArrayElementAtIndex(listSelectedIndex);
						p_selectedObjects = p_selectedList.FindPropertyRelative("objects");
						// it is the first time the script has been selected setup its counters
						inlistCountFromInput = p_selectedObjects.arraySize;
						lastKnownInlistCount = inlistCountFromInput;
					}
				}
				else if( listSelectedIndex < processorLists.Count && processorLists.Count > 0 )
				{
					p_selectedList = p_lists.GetArrayElementAtIndex(listSelectedIndex);
					p_selectedObjects = p_selectedList.FindPropertyRelative("objects");
				}



				// popup for selected list to display objects for, this will include [Create New]
				strArray = GetListNameStrings(processorLists);
				r = EditorGUILayout.GetControlRect();
				int activeSelectedIndex = EditorGUI.Popup(r, listSelectedIndex, strArray);

				if( activeSelectedIndex == strArray.Length-1 ) // [Create New] has been selected
				{
					// increase list size via serialisedProperty, then assign it the processor list size
					p_lists.arraySize += 1;
					serializedObject.ApplyModifiedProperties();

					processorLists[processorLists.Count-1] = new ProcessableList("New List");
					activeSelectedIndex = processorLists.Count-1;
				}

				if( activeSelectedIndex < processorLists.Count && activeSelectedIndex >= 0 ) // valid processablelist has been selected
				{
					// check if the selected list has changed
					p_selectedList = p_lists.GetArrayElementAtIndex(activeSelectedIndex);
					if( listSelectedIndex != activeSelectedIndex )
					{
						// new selection
						inlistCountFromInput = p_selectedObjects.arraySize;
						lastKnownInlistCount = inlistCountFromInput;
					}
					listSelectedIndex = activeSelectedIndex;

					SerializedProperty p_listName = p_selectedList.FindPropertyRelative("listName");

					string pre = p_listName.stringValue;
					if( p_defaultList.stringValue == pre )
					{
						r = EditorGUILayout.GetControlRect();
						EditorGUI.PropertyField( r, p_listName );
						string post = p_listName.stringValue;
						if( post != pre )
						{
							// name changed of default, set that too
							p_defaultList.stringValue = post;
						}
					}
					else
					{
						r = EditorGUILayout.GetControlRect();
						EditorGUI.PropertyField( r, p_listName );
					}

					FoldoutListObjects(p_shouldProcessObjectsFoldOut);

					r = EditorGUILayout.GetControlRect();
					// adjust buttonR so it taking indent into account
					if( GUI.Button( r, "Delete List \"" + p_listName.stringValue + "\"" ) )
					{
						// popup with are you sure?
						if( EditorUtility.DisplayDialog( "Delete this List?",
						                            "Are you sure you want delete " + p_listName.stringValue + "?",
						                            "Delete", "Do Not Delete" ) )
						{
							p_lists.DeleteArrayElementAtIndex(activeSelectedIndex);
							serializedObject.ApplyModifiedProperties();
							return;
						}
					}
				}
			}

			EditorGUI.indentLevel--;

			r = EditorGUILayout.GetControlRect();
			HeadRect(r);
			GUI.contentColor = conColH; p_showProcessSettings.boolValue = EditorGUI.ToggleLeft( r, "Process Settings", p_showProcessSettings.boolValue ); GUI.contentColor = Color.white;
			EditorGUI.indentLevel++;

			if( p_showProcessSettings.boolValue )
			{
				r.y += 16; r.height+=2;
				BodyRect(r, 3);

				r = EditorGUILayout.GetControlRect();
				EditorGUI.PropertyField( r, p_stopOnListChange );
				r = EditorGUILayout.GetControlRect();
				EditorGUI.PropertyField( r, p_whenInSequence );
				r = EditorGUILayout.GetControlRect();
				EditorGUI.PropertyField( r, p_cycleToNextEachTrigger );
			}

			EditorGUI.indentLevel--;
			r = EditorGUILayout.GetControlRect();
			HeadRect(r);
			GUI.contentColor = conColH; p_showRestrictionSettings.boolValue = EditorGUI.ToggleLeft( r, "Restrict Settings", p_showRestrictionSettings.boolValue ); GUI.contentColor = Color.white;
			EditorGUI.indentLevel++;

			if( p_showRestrictionSettings.boolValue )
			{
				r.y += 16; r.height+=2;
				if( p_shouldRestrictorsFoldOut.boolValue )
					BodyRect(r, 3 + script.restrictions.Count);
				else
					BodyRect(r, 2);

				RestrictorsFoldout(p_shouldRestrictorsFoldOut);

				r = EditorGUILayout.GetControlRect();
				EditorGUI.PropertyField( r, p_processLimitation );

				switch( (Processor.EMaximumProcessSetting)p_processLimitation.enumValueIndex )
				{
				case Processor.EMaximumProcessSetting.maximumCount:
					r = EditorGUILayout.GetControlRect();
					EditorGUI.PropertyField( r, p_maximumProcessCount );
					break;
				case Processor.EMaximumProcessSetting.maximumCountResetOnEnable:
					r = EditorGUILayout.GetControlRect();
					EditorGUI.PropertyField( r, p_maximumProcessCount );
					break;
				}
			}

			serializedObject.ApplyModifiedProperties();
		}


		string[] GetListNameStrings( List<ProcessableList> processorsLists )
		{
			if( processorsLists.Count == 0 )
				return new string[] {"", "[Create New]"};
			else
			{
				string[] strArray = new string[processorsLists.Count+1];
				
				for( int i = 0; i <= processorsLists.Count; ++i )
				{
					if( i == processorsLists.Count )
						strArray[i] = "[Create New]";
					else
						strArray[i] = processorsLists[i].listName;
				}
				return strArray;
			}
		}


		bool FoldoutListObjects( SerializedProperty p_shouldProcessObjectsFoldOut )
		{
			// Drag and Drop objects
			Rect foldoutArea = EditorGUILayout.GetControlRect();
			List<ObjectToAdd> objectsToAdd = DragAndDropOntoArea(foldoutArea, p_selectedObjects, typeof(ProcessableObject));
			if( objectsToAdd.Count > 0 )
			{
				// TODO if there are any with siblings to add a pop window

				// increase array size
				p_selectedObjects.arraySize += objectsToAdd.Count;
				serializedObject.ApplyModifiedProperties();
				
				int addIndex = 0;
				ProcessableList aList = script.processableLists[listSelectedIndex];
				for( int i = p_selectedObjects.arraySize - objectsToAdd.Count; i < p_selectedObjects.arraySize; ++i )
				{
					aList.SetAtIndex( i, (ProcessableObject)objectsToAdd[addIndex].obj, true );
					addIndex++;
				}

				serializedObject.ApplyModifiedProperties();
				return true;
			}
			
			
			EditorGUI.indentLevel+=2;
			FoldoutProcessableObjects( ref p_shouldProcessObjectsFoldOut, ref inlistCountFromInput, ref lastKnownInlistCount, foldoutArea, p_selectedObjects, 
			        new GUIContent("Elements", "ProcessableObjects in the list to be processed") );
			EditorGUI.indentLevel-=2;

			return false;
		}

		bool RestrictorsFoldout( SerializedProperty isFolded )
		{
			Rect foldoutArea = EditorGUILayout.GetControlRect();
			List<ObjectToAdd> objectsToAdd = DragAndDropOntoArea(foldoutArea, p_restrictions, typeof(Restrictor));
			if( objectsToAdd.Count > 0 )
			{
				// TODO if there are any with siblings to add a pop window
				
				p_restrictions.arraySize += objectsToAdd.Count;
				serializedObject.ApplyModifiedProperties();
				
				int addIndex = 0;
				for( int i = p_restrictions.arraySize - objectsToAdd.Count; i < p_restrictions.arraySize; ++i )
				{
					p_restrictions.GetArrayElementAtIndex(i).objectReferenceValue = objectsToAdd[addIndex].obj;
					addIndex++;
				}

				serializedObject.ApplyModifiedProperties();
				return true;
			}
			
			
			EditorGUI.indentLevel++;
			StandardFoldout( ref isFolded, ref restrictorsCountFromInput, ref lastKnownRestrictorCount, foldoutArea, p_restrictions, 
			        new GUIContent("Restrictors", "Restrictions on if the processor should process or not when triggered.") );
			EditorGUI.indentLevel--;
			
			return false;
		}

		void FoldoutProcessableObjects( ref SerializedProperty isFolded, ref int tempInputCount, ref int lastKnownArrayCount, Rect area, SerializedProperty arrayProp,  GUIContent foldoutGUI )
		{
			isFolded.boolValue = EditorGUI.Foldout( area, isFolded.boolValue, foldoutGUI, true);
			if( isFolded.boolValue == false )
				return;
			
			// list size int box
			GUI.SetNextControlName(arrayProp.name);
			Rect r = EditorGUILayout.GetControlRect();
			tempInputCount = EditorGUI.IntField(r, new GUIContent("Count", "Array size"), tempInputCount);
			if( GUI.GetNameOfFocusedControl() == arrayProp.name )
			{
				Event e = Event.current;
				if(e.keyCode == KeyCode.Return )
				{
					arrayProp.arraySize = tempInputCount;
				}
			}
			
			// List all the objects
			for (int i = 0; i < arrayProp.arraySize; i++)
			{
				SerializedProperty elementProperty = arrayProp.GetArrayElementAtIndex(i);

				r = EditorGUILayout.GetControlRect();
				if( elementProperty.objectReferenceValue != null )
				{
					ProcessableObject proObj = elementProperty.objectReferenceValue as ProcessableObject;

					if( proObj != null )
					{
						Actor actorRef = proObj as Actor;

						Rect mainRect = new Rect(r);
						Rect waitToggleRect = new Rect(r);

						if( actorRef != null )
						{
							mainRect.width = r.width-40;
							waitToggleRect.width = 40;
							waitToggleRect.x = mainRect.x + mainRect.width;

							EditorGUI.PropertyField(mainRect, elementProperty);

							int sequencialIndex = -1;
							SerializedProperty sequencialListProp = p_selectedList.FindPropertyRelative("areSequencial");
							for (int s = 0; s < sequencialListProp.arraySize; s++)
							{
								SerializedProperty indexProb = sequencialListProp.GetArrayElementAtIndex(s);
								if( indexProb.objectReferenceValue == proObj )
								{
									sequencialIndex = s;
									break;
								}
							}

							bool newVal = GUI.Toggle(waitToggleRect, sequencialIndex >= 0, "wait");
							if( newVal != (sequencialIndex >= 0) )
							{
								if( newVal )
								{	// add object to the sequecial list
									sequencialListProp.arraySize += 1;
									serializedObject.ApplyModifiedProperties();

									SerializedProperty prop = sequencialListProp.GetArrayElementAtIndex(sequencialListProp.arraySize-1);
									prop.objectReferenceValue = proObj;
								}
								else
								{	// remove the object from the sequencial
									sequencialListProp.DeleteArrayElementAtIndex(sequencialIndex);
								}
							}
						}
						else
						{
							EditorGUI.PropertyField(mainRect, elementProperty);
						}
					}
				}
				else
					EditorGUI.PropertyField(r, elementProperty);
			}
			
			if( arrayProp.arraySize != lastKnownArrayCount ) // the list has been modified by other means
			{
				tempInputCount = arrayProp.arraySize;
				lastKnownArrayCount = tempInputCount;

				// clean the sequencial list of any missing sequencial connections to objects that nolonger exist
				ProcessableList aList = script.processableLists[listSelectedIndex];
				aList.CleanSequencial();
			}
		}


		void StandardFoldout( ref SerializedProperty isFolded, ref int tempInputCount, ref int lastKnownArrayCount, Rect area, SerializedProperty arrayProp,  GUIContent foldoutGUI )
		{
			isFolded.boolValue = EditorGUI.Foldout( area, isFolded.boolValue, foldoutGUI, true);
			if( isFolded.boolValue == false )
				return;

			// list size int box
			GUI.SetNextControlName(arrayProp.name);
			Rect r = EditorGUILayout.GetControlRect();
			tempInputCount = EditorGUI.IntField(r, new GUIContent("Count", "Array size"), tempInputCount);
			if( GUI.GetNameOfFocusedControl() == arrayProp.name )
			{
				Event e = Event.current;
				if(e.keyCode == KeyCode.Return )
				{
					arrayProp.arraySize = tempInputCount;
				}
			}

			// List all the objects
			for (int i = 0; i < arrayProp.arraySize; i++)
			{
				SerializedProperty elementProperty = arrayProp.GetArrayElementAtIndex(i);
				r = EditorGUILayout.GetControlRect();
				EditorGUI.PropertyField(r, elementProperty); 
			}

			if( arrayProp.arraySize != lastKnownArrayCount ) // the objects list has been modified by other means
			{
				tempInputCount = arrayProp.arraySize;
				lastKnownArrayCount = tempInputCount;
			}
		}

		// manage the dragging and dropping onto the foldout area
		List<ObjectToAdd> DragAndDropOntoArea( Rect area, SerializedProperty objects, System.Type ofType )
		{
			Event current = Event.current;
			List<ObjectToAdd> objectsToAdd = new List<ObjectToAdd>();
			if ( ! area.Contains( current.mousePosition ) )
				return objectsToAdd;

			switch (current.type)
			{
			case EventType.DragUpdated:
				foreach (Object obj in DragAndDrop.objectReferences)
				{
					if( obj.GetType().IsSubclassOf(ofType) )
					{
						DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
						break;
					}
					else if( obj.GetType() == typeof(GameObject) )
					{
						Component gotComponent = ((GameObject)obj).GetComponent(ofType);
						if( gotComponent != null )
						{
							DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
							break;
						}
					}
				}
				return objectsToAdd;

			case EventType.DragPerform:
				DragAndDrop.AcceptDrag();
				foreach (Object obj in DragAndDrop.objectReferences)
				{
					if( obj.GetType().IsSubclassOf(ofType) )
					{
						objectsToAdd.Add(new ObjectToAdd(obj,false));
					}
					else if( obj.GetType() == typeof(GameObject) )
					{
						Component[] gotComponents = ((GameObject)obj).GetComponents(ofType);
						foreach( Component c in gotComponents )
						{
							if( c != null )
								objectsToAdd.Add(new ObjectToAdd(c, gotComponents.Length > 1));
						}
					}
				}
				return objectsToAdd;
			}

			return objectsToAdd;
		}


		void OnInspectorUpdate()
		{
			Repaint();
		}
	}
}
	