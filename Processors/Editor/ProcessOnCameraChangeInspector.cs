﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using TogglePlay;
using TogglePlay.Processors;

namespace InspectorEditor.Core
{
	[CustomEditor(typeof(ProcessOnCameraChange),true), CanEditMultipleObjects]
	public class ProcessOnCameraChangeInspector : ProcessorInspector
	{
		SerializedProperty onChangeMask;
		public override void OnEnable()
		{
			base.OnEnable();
			onChangeMask = serializedObject.FindProperty("onChangeMask");
		}

		ProcessOnCameraChange camScript = null;

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			serializedObject.Update();

			if( camScript == null )
				camScript = serializedObject.targetObject as ProcessOnCameraChange;

			onChangeMask.intValue = (int)((ProcessOnCameraChange.EBeginOnCameraChange)EditorGUILayout.EnumMaskField(
				"On Change Mask", (ProcessOnCameraChange.EBeginOnCameraChange)onChangeMask.intValue));

			serializedObject.ApplyModifiedProperties();
		}
	}
}
	