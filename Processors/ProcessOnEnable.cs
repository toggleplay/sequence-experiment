using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Enable")]
	public class ProcessOnEnable : Processor
	{
		protected override void OnEnable()
		{
			base.OnEnable();
			RunAsCoroutine();
		}
	}
}