using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Trigger")]
	public class ProcessOnTrigger : Processor
	{
		public string triggerTag = "";
		public enum ETrigger { enter = 0, stay, exit };
		public ETrigger trigger = ETrigger.enter;

		public virtual void OnTriggerEnter( Collider col )
		{
			if( triggerTag == col.tag && trigger == ETrigger.enter )
				RunAsCoroutine();
		}
		public virtual void OnTriggerStay( Collider col )
		{
			if( triggerTag == col.tag && trigger == ETrigger.stay )
				RunAsCoroutine();
		}
		public virtual void OnTriggerExit( Collider col )
		{
			if( triggerTag == col.tag && trigger == ETrigger.exit )
				RunAsCoroutine();
		}
	}
}