using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Update")]
	public class ProcessOnUpdate : Processor
	{
		public enum EProcessOn { update = 0, lateUpdate, fixedUpdate };
		public EProcessOn processOn = EProcessOn.update;

		void Update()
		{
			if( processOn == EProcessOn.update )
				RunAsCoroutine();
		}

		void LateUpdate()
		{
			if( processOn == EProcessOn.lateUpdate )
				RunAsCoroutine();
		}

		void FixedUpdate()
		{
			if( processOn == EProcessOn.fixedUpdate )
				RunAsCoroutine();
		}
	}
}