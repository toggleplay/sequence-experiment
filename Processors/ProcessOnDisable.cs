using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Disable")]
	public class ProcessOnDisable : Processor
	{
		void OnDisable()
		{
			RunAsCoroutine();
		}
	}
}