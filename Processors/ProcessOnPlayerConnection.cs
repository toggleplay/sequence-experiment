using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Player Connected")]
	public class ProcessOnPlayerConnection : Processor
	{
		public enum EProcessOn { connected = 0, disconnected };
		public EProcessOn processOn = EProcessOn.connected;

		void OnPlayerConnected()
		{
			if( processOn == EProcessOn.connected )
				RunAsCoroutine();
		}

		void OnPlayerDisconnected()
		{
			if( processOn == EProcessOn.disconnected )
				RunAsCoroutine();
		}
	}
}