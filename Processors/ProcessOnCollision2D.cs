using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Collision 2D")]
	public class ProcessOnCollision2D : Processor
	{
		public string triggerTag = "";
		public enum ETrigger { enter = 0, stay, exit };
		public ETrigger trigger = ETrigger.enter;

		public virtual void OnCollisionEnter2D( Collision2D col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.enter )
				RunAsCoroutine();
		}
		public virtual void OnCollisionStay2D( Collision2D col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.stay )
				RunAsCoroutine();
		}
		public virtual void OnCollisionExit2D( Collision2D col )
		{
			if( triggerTag == col.gameObject.tag && trigger == ETrigger.exit )
				RunAsCoroutine();
		}
	}
}