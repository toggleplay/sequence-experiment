using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Application Events")]
	public class ProcessOnApplicationEvent : Processor
	{
		// TODO editor script for this
		public enum EProcessOn  { onFocus = 0, onQuit, onPause };
		public enum EFocusState { intoFocus = 0, outOfFocus };
		public enum EPauseState { intoPause = 0, outOfPause };

		public EProcessOn  processOn = EProcessOn.onPause;
		public EFocusState onFocalState = EFocusState.intoFocus;
		public EPauseState onPause = EPauseState.intoPause;

		void OnApplicationFocus( bool focalStatus )
		{
			if( processOn == EProcessOn.onFocus )
			{
				if( onFocalState == EFocusState.intoFocus && focalStatus == true )
					RunAsCoroutine();
				else if( onFocalState == EFocusState.outOfFocus && focalStatus == false )
					RunAsCoroutine();
			}
		}

		void OnApplicationQuit( )
		{
			if( processOn == EProcessOn.onQuit )
				RunAsCoroutine();
		}
		
		void OnApplicationPause( bool enterPause )
		{
			if( processOn == EProcessOn.onPause )
			{
				if( onPause == EPauseState.intoPause && enterPause == true )
					RunAsCoroutine();
				else if( onPause == EPauseState.outOfPause && enterPause == false )
					RunAsCoroutine();
			}
		}
	}
}