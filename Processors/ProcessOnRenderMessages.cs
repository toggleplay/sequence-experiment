using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Render Messages")]
	public class ProcessOnRenderMessages : Processor
	{
		public enum EProcessOn { preRender = 0, preCull, postRender, willRenderObject, renderObject };
		public EProcessOn processOn = EProcessOn.preRender;

		void OnPreRender()
		{
			if( processOn == EProcessOn.preRender )
				RunAsCoroutine();
		}

		void OnPostRender()
		{
			if( processOn == EProcessOn.postRender )
				RunAsCoroutine();
		}

		void OnPreCull()
		{
			if( processOn == EProcessOn.preCull )
				RunAsCoroutine();
		}

		void OnWillRenderObject()
		{
			if( processOn == EProcessOn.willRenderObject )
				RunAsCoroutine();
		}

		void OnRenderObject()
		{
			if( processOn == EProcessOn.renderObject )
				RunAsCoroutine();
		}
	}
}