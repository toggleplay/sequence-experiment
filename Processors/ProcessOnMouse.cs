using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Mouse")]
	public class ProcessOnMouse : Processor
	{
		public enum EMouseEvent { Enter = 0, Exit, Over, Up, Down, Drag, UpAsButton };
		public EMouseEvent onMouse = EMouseEvent.Down;

		void OnMouseDown()
		{
			if( onMouse == EMouseEvent.Down )
				RunAsCoroutine();
		}

		void OnMouseDrag()
		{
			if( onMouse == EMouseEvent.Drag )
				RunAsCoroutine();
		}

		void OnMouseEnter()
		{
			if( onMouse == EMouseEvent.Enter )
				RunAsCoroutine();
		}

		void OnMouseExit()
		{
			if( onMouse == EMouseEvent.Exit )
				RunAsCoroutine();
		}

		void OnMouseOver()
		{
			if( onMouse == EMouseEvent.Over )
				RunAsCoroutine();
		}

		void OnMouseUp()
		{
			if( onMouse == EMouseEvent.Up )
				RunAsCoroutine();
		}

		void OnMouseUpAsButton()
		{
			if( onMouse == EMouseEvent.UpAsButton )
				RunAsCoroutine();
		}
	}
}