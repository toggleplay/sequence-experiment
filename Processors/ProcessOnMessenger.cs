using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Messenger")]
	public class ProcessOnMessenger : Processor
	{
		public Messengers.EventMessenger[] messengers = new TogglePlay.Messengers.EventMessenger[0];

		void EventTrigger()
		{
			RunAsCoroutine();
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			foreach( Messengers.EventMessenger m in messengers )
			{
				if( m != null )
					m.onSent += EventTrigger;
			}
		}

		void OnDisable()
		{
			foreach( Messengers.EventMessenger m in messengers )
			{
				if( m != null )
					m.onSent -= EventTrigger;
			}
		}
	}
}