using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Destroy")]
	public class ProcessOnDestroy : Processor
	{
		void OnDestroy()
		{
			RunAsCoroutine();
		}
	}
}