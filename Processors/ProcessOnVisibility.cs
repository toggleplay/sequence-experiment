using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Visibility")]
	public class ProcessOnVisibility : Processor
	{
		public enum EVisibleState { onVisible = 0, onInvisible };
		public EVisibleState onState = EVisibleState.onVisible;

		void OnBecameVisible()
		{
			if( onState == EVisibleState.onVisible )
				RunAsCoroutine();
		}

		void OnBecameInvisible()
		{
			if( onState == EVisibleState.onInvisible )
				RunAsCoroutine();
		}
	}
}