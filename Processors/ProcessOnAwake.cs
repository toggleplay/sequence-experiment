using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Awake")]
	public class ProcessOnAwake : Processor
	{
		void Awake()
		{
			RunAsCoroutine();
		}
	}
}