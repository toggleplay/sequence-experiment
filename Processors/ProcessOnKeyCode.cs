using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On KeyCode")]
	public class ProcessOnKeyCode : Processor
	{
		public enum EKeyState { down = 0, held, up };

		public KeyCode onCode = KeyCode.Space;
		public EKeyState onState = EKeyState.down;

		void Update()
		{
			switch( onState )
			{
			case EKeyState.down:
				if( Input.GetKeyDown(onCode) )
					RunAsCoroutine();
				break;
			case EKeyState.up:
				if( Input.GetKeyUp(onCode) )
					RunAsCoroutine();
				break;
			case EKeyState.held:
				if( Input.GetKey(onCode) )
					RunAsCoroutine();
				break;
			}
		}
	}
}