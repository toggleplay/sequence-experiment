using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Server Connectivity")]
	public class ProcessOnServerConnectivity : Processor
	{
		public enum EProcessOn { connectedToServer = 0, serverInitialised, disconnectedFromServer };
		public EProcessOn processOn = EProcessOn.connectedToServer;

		void OnConnectedToServer()
		{
			if( processOn == EProcessOn.connectedToServer )
				RunAsCoroutine();
		}

		void OnServerInitialised()
		{
			if( processOn == EProcessOn.serverInitialised )
				RunAsCoroutine();
		}

		void OnDisconnectedFromServer(NetworkDisconnection disConInfo)
		{
			if( processOn == EProcessOn.disconnectedFromServer )
				RunAsCoroutine();
		}
	}
}