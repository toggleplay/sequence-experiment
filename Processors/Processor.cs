﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using TogglePlay.Processors;
using TogglePlay.Restrictions;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/Processor Standard")]
	public class Processor : ProcessableObject
	{
		public enum EMaximumProcessSetting { noLimit = 0, maximumCount, maximumCountResetOnEnable };
		public enum EBeginInProcess { ignoreProcessCall = 0, stopActiveFirst };

		[HideInInspector] public bool shouldRestrictorsFoldOut = false;
		[HideInInspector] public bool shouldProcessObjectsFoldOut = false;
		[HideInInspector] public bool showProcessingLists = true;
		[HideInInspector] public bool showProcessSettings = true;
		[HideInInspector] public bool showRestrictionSettings = true;
		
		[HideInInspector, SerializeField] private string defaultList = "";
		private int currentList = -1;

		// TODO onList change do enum X Y Z
		[HideInInspector] public bool stopOnListChange = true;
		[HideInInspector] public EBeginInProcess whenInSequence = EBeginInProcess.ignoreProcessCall;
			
		[HideInInspector] public List<ProcessableList> processableLists = new List<ProcessableList>();
		[HideInInspector] public bool cycleToNextEachTrigger = false;
		
		[HideInInspector] public List<Restrictor> restrictions = new List<TogglePlay.Restrictions.Restrictor>();
		[HideInInspector] public EMaximumProcessSetting processLimitation = EMaximumProcessSetting.noLimit;
		[HideInInspector] public int maximumProcessCount = 0;
		private int processedCount = 0;
		
		public event TogglePlay.Delegates.BlankDelegate onListCompletion = null;
		
		//*****************************************************************


		protected virtual void OnEnable()
		{
			if( processLimitation == EMaximumProcessSetting.maximumCountResetOnEnable )
				processedCount = 0;
		}
		
		public void ResetMaximumProcessCount()
		{
			processedCount = 0;
		}
		public void SetMaximumProcessorCount( int count )
		{
			maximumProcessCount = count;
		}
		public void SetMaximumProcessorCount( int count, bool resetCounter )
		{
			maximumProcessCount = count;
			if( resetCounter )
				processedCount = 0;
		}
		
		public void SetToList( string listName )
		{
			SetToList(listName, false);
		}
		public virtual void SetToList( string listName, bool autoPlay )
		{
			if( stopOnListChange == true )
			{
				if( currentList >= 0 && currentList < processableLists.Count )
				{
					if( processableLists[currentList].isInRoutine )
					{
						processableLists[currentList].Stop();
					}
				}
			}
			
			SetCurrentList( listName );

			if( autoPlay )
				RunAsCoroutine();
		}
		protected void SetCurrentList( string listName )
		{
			for( int i = 0; i < processableLists.Count; ++i )
			{
				if( processableLists[i].listName == listName )
				{
					currentList = i;
					break;
				}
			}
		}
		
		public void Pause()
		{
			if( currentList < 0 && currentList >= processableLists.Count )
				return;
			if( processableLists[currentList].isInRoutine )
				processableLists[currentList].Pause();
		}
		
		public void Resume()
		{
			if( currentList < 0 && currentList >= processableLists.Count )
				return;
			if( processableLists[currentList].isInRoutine )
				processableLists[currentList].Resume();
		}

		public override Coroutine RunAsCoroutine()
		{
			return RunAsCoroutine(null);
		}
		public override IEnumerator RunAsEnumerator()
		{
			return Run(null);
		}
		public override IEnumerator RunAsEnumerator(TogglePlay.Delegates.VoidDelegate endedCallback)
		{
			return Run(endedCallback);
		}

		[ContextMenu("Begin Process")]
		public override Coroutine RunAsCoroutine( TogglePlay.Delegates.VoidDelegate endedCallback )
		{
#if UNITY_EDITOR
			if( ! Application.isPlaying )
			{
				Debug.Log("Application is not running");
				return null;
			}
#endif
			return StartCoroutine( Run(endedCallback) );
		}


		protected virtual IEnumerator Run( TogglePlay.Delegates.VoidDelegate endedCallback )
		{
			// check for any restrictions on if it should 
			foreach( Restrictor r in restrictions )
			{
				if( r == null )
					continue;
				if( r.isRestricted )
					yield break;
			}
			
			if( processLimitation != EMaximumProcessSetting.noLimit && processedCount >= maximumProcessCount )
				yield break;
			
			if( currentList >= 0 && currentList < processableLists.Count )
			{
				if( processableLists[currentList].isInRoutine )
				{
					if( whenInSequence == EBeginInProcess.stopActiveFirst )
					{
						// the list is running already, stop them and wait a frame for them to finish
						processableLists[currentList].Stop(); // Beware, the canceled callback will be called in the next update loop of the Objects
					}
					else if( whenInSequence == EBeginInProcess.ignoreProcessCall )
					{
						yield break;
					}
				}
				
				if( cycleToNextEachTrigger == true )
				{
					currentList++;
					if( currentList >= processableLists.Count )
						currentList = 0;
				}
			}
			
			if( currentList == -1 )
			{
				SetToList(defaultList);
				
				// No default list found, try to set to first list in lists
				if( currentList == -1 )
				{
					if( processableLists.Count == 0 )
					{
						Debug.LogWarning( "No lists for : " + this.name );
						yield break;
					}
					else
					{
						Debug.LogWarning( "No default List found, setting to first list : " + processableLists[0].listName );
						currentList = 0;
					}
				}
			}

			processedCount++;

			// tell list to process its objects
			IEnumerator e = processableLists[currentList].Process();
			while( e.MoveNext() )
				yield return e;

			if( endedCallback != null )
				endedCallback();
			if( onListCompletion != null )
				onListCompletion();
		}


		public int GetObjectCountInList( string listName )
		{
			if( listName == "" )
				return 0;
			for( int i = 0; i < processableLists.Count; ++i )
			{
				if( processableLists[i].listName == listName )
					return processableLists[i].Count;
			}
			return 0;
		}
	}
}
