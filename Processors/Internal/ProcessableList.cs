using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using TogglePlay;
using TogglePlay.Actors;

namespace TogglePlay.Processors
{
	[System.Serializable]
	public class ProcessableList
	{
		public class ProcessTracker
		{
			public LinkedList<ProcessableObject> objects = new LinkedList<ProcessableObject>();
			public bool paused = false;
			public bool toStop = false;
		}

		public ProcessableList()
		{
		}

		public ProcessableList( string name )
		{
			listName = name;
		}

		//******************************************

		public string listName = "";
		[SerializeField] private List<ProcessableObject> objects = new List<ProcessableObject>();
		[SerializeField] private List<ProcessableObject> areSequencial = new List<ProcessableObject>();

		List<ProcessTracker> activeProcesses = new List<ProcessTracker>();

		public int Count
		{
			get{ return objects.Count; }
		}

		public void SetAsSequencial( ProcessableObject a )
		{
			areSequencial.Add(a);
		}
		public void SetAsNonsequencial( ProcessableObject a )
		{
			areSequencial.Remove(a);
		}

		public bool IsSequencial( ProcessableObject a )
		{
			return areSequencial.Contains(a);
		}
		public void CleanSequencial()
		{
			for( int i=areSequencial.Count-1; i>= 0; --i )
			{
				if( ! objects.Contains(areSequencial[i] ) )
				{
					areSequencial.RemoveAt(i);
				}
			}
		}

		public bool SetAtIndex( int index, ProcessableObject setToObj, bool sequencial )
		{
			if( index < 0 || objects.Count <= index )
				return false;
			if( objects[index] != null )
			{
				// TODO check if its being processed
			}

			objects[index] = setToObj;
			if( sequencial && areSequencial.Contains(setToObj) == false )
				areSequencial.Add(setToObj);

			return true;
		}


		public bool isInRoutine
		{
			get{ return activeProcesses.Count > 0; }
		}

		
		// keep actor list private with add/remove functions preventing access during a process
		public void Add(ProcessableObject a)
		{
			// add to end of any processees
			foreach( ProcessTracker p in activeProcesses )
			{
				p.objects.AddLast(a);
			}

			objects.Add(a);
		}
		public void Remove(ProcessableObject a)
		{
			// TODO remove from processes

			objects.Remove(a);
		}
		
		public void Stop()
		{
			foreach( ProcessTracker p in activeProcesses )
			{
				p.toStop = true;
			}
		}

		public void Pause()
		{
			foreach( ProcessTracker p in activeProcesses )
			{
				p.paused = true;
			}
		}

		public void Resume()
		{
			foreach( ProcessTracker p in activeProcesses )
			{
				p.paused = false;
			}
		}

		public IEnumerator Process()
		{
			if( objects.Count == 0 )
				yield break;

			LinkedList<ProcessableObject> toProcess = new LinkedList<ProcessableObject>();
			foreach( ProcessableObject a in objects )
			{
				if( a == null )
					continue;
				toProcess.AddLast( a );
			}

			if( toProcess.Count == 0 )
				yield break;
			ProcessTracker tracker = new ProcessTracker();
			tracker.objects = toProcess;

			activeProcesses.Add( tracker );

			List<IEnumerator> enumerators = new List<IEnumerator>();
			LinkedListNode<ProcessableObject> n = toProcess.First;
			IEnumerator activeEnumerator = null;

			// setup starting state
			while( n != null && activeEnumerator == null )
			{
				if( IsSequencial(n.Value) == false )
				{
					enumerators.Add( n.Value.RunAsEnumerator() );
					n = n.Next;
				}
				else
				{
					activeEnumerator = n.Value.RunAsEnumerator();
					break;
				}
			}

			while( n != null )
			{
				// if its TODO stopped or paused, handle
				if( tracker.paused )
				{
					yield return null;
					continue;
				}

				// move on all enumerators
				for( int i=enumerators.Count-1; i>=0; --i )
				{
					if( ! enumerators[i].MoveNext() )
					{
						// remove it
						enumerators.RemoveAt(i);
					}
				}

				// move the current one forward
				if( ! activeEnumerator.MoveNext() )
				{
					n = n.Next;
					if( n == null )
						break;
					else if( IsSequencial(n.Value) == false )
						enumerators.Add( n.Value.RunAsEnumerator() );
					else
						activeEnumerator = n.Value.RunAsEnumerator();
				}
				else
				{
					// TODO check for anything to break the  activeEnumerator etc
				}

				yield return null;
			}

			// finish up
			while( enumerators.Count > 0 )
			{
				// if its TODO stopped or paused, handle
				if( tracker.paused )
				{
					yield return null;
					continue;
				}

				// move on all enumerators
				for( int i=enumerators.Count-1; i>=0; --i )
				{
					if( ! enumerators[i].MoveNext() )
					{
						// remove it
						enumerators.RemoveAt(i);
					}
				}

				yield return null;
			}

			activeProcesses.Remove( tracker );
		}
	}

}
