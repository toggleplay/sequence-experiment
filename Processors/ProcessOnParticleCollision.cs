using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Particle Collision")]
	public class ProcessOnParticleCollision : Processor
	{
		void OnParticleCollision()
		{
			RunAsCoroutine();
		}
	}
}