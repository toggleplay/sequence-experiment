using UnityEngine;
using System.Collections;

namespace TogglePlay.Processors
{
	[AddComponentMenu("Toggle Play/Processors/On Controller Collider Hit")]
	public class ProcessOnControllerColliderHit : Processor
	{
		public string triggerTag = "";

		void OnControllerColliderHit(ControllerColliderHit hit)
		{
			if( hit.collider.tag == triggerTag )
				RunAsCoroutine();
		}
	}
}